# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This will store the Java API code
* Currently contains basic setup and dropwizard hello world tutorial (This is only temporary) 

### How do I get set up? ###

* Clone the repo
* Import the project into eclipse as an existing project
* Make it into a Maven Project
* Download Java JDK 1.8.0_25 and use that for the java execution environment (Set in preferences along with execution environement to J2SE-1.5 and compiler to1.8  Java 6 should still be used for your Mac but you can point to the java 8 for development)
* Run in command line to build jar: mvn package or use the eclipse clean/build.
* Run in command line to run server: java -jar target/igolf-0.0.1-SNAPSHOT.jar server src/main/config/config.yml or add server src/main/config/config.yml to run configurations program arguments and click run for StrokemasterApp
* Verify that it's running: http://localhost:9000/hello-world


### Who do I talk to? ###

* Message Greg if you have any questions
* Use this site as a resource and for reference to the demo http://dropwizard.io/getting-started.html