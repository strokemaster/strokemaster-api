
package com.strokemaster.strokemasterService.core;

import java.util.ArrayList;

import org.json.JSONObject;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Object that represents data from a given hole
 * @author Greg Morano
 *
 */
public class CourseRound {
	
	//private String roundId;
	@JsonProperty("CourseId")
	private String courseId;
	@JsonProperty("UserId")
	private String userId;
	@JsonProperty("TotalScore")
	private int totalScore;
	@JsonProperty("Score")
	private String score;
	@JsonProperty("TotalPar")
	private int totalPar;
	@JsonProperty("Par")
	private String par;
	@JsonProperty("StartHole")
	private int startHole;
	@JsonProperty("Holes")
	private ArrayList<HoleShots> holes;


	/**
     * default constructor
     */
    public CourseRound() {
        // Jackson deserialization
    }
	
	public CourseRound(JSONObject data){
		//this.roundId = 1;
		try{
			
			this.userId = data.getString("userId");
			this.courseId = data.getString("courseId");
			this.par = data.getString("par");
			this.totalScore = data.getInt("totalScore");
			this.totalPar = data.getInt("totalPar");
			this.score = data.getString("score");
			this.holes = new ArrayList<HoleShots>();
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}

	/*
	 * getters and setter
	 */


	public String getCourseId() {
		return courseId;
	}


	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}


	public String getUserId() {
		return userId;
	}


	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public int getTotalScore() {
		return totalScore;
	}

	public void setTotalScore(int totalScore) {
		this.totalScore = totalScore;
	}

	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}

	public String getPar() {
		return par;
	}

	public void setPar(String par) {
		this.par = par;
	}

	public int getStartHole() {
		return startHole;
	}


	public void setStartHole(int startHole) {
		this.startHole = startHole;
	}


	public ArrayList<HoleShots> getHoles() {
		return holes;
	}


	public void setHoles(ArrayList<HoleShots> holes) {
		this.holes = holes;
	}

	public int getTotalPar() {
		return totalPar;
	}

	public void setTotalPar(int totalPar) {
		this.totalPar = totalPar;
	}


	
}