
package com.strokemaster.strokemasterService.core;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Object that represents data from a given hole
 * @author Greg Morano
 *
 */
public class GolfBag {
	@JsonProperty("Email")
	private String email;
	@JsonProperty("Clubs")
	private ArrayList<String> clubs;
	
	//default constructor
	public GolfBag(){
		
	}
	
	public GolfBag(String email, ArrayList<String> clubs) {
		super();
		this.email = email;
		this.clubs = clubs;
	}
	
	
	//getters and setters
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public ArrayList<String> getClubs() {
		return clubs;
	}
	public void setClubs(ArrayList<String> clubs) {
		this.clubs = clubs;
	}
	

}

