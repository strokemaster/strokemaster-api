
package com.strokemaster.strokemasterService.core;


/**
 * Object that represents data from a given hole
 * @author Greg Morano
 *
 */
public class Leaderboard {
	
	private double avgScore;
	private int numRounds;
	private String userName;


	public Leaderboard(double avg, int rounds, String user){
		this.avgScore = avg;
		this.numRounds = rounds;
		this.userName = user;
		
	}

	/*
	 * getters and setter
	 */
	
	public double getAvgScore() {
		return avgScore;
	}


	public void setAvgScore(double avgScore) {
		this.avgScore = avgScore;
	}


	public int getNumRounds() {
		return numRounds;
	}


	public void setNumRounds(int numRounds) {
		this.numRounds = numRounds;
	}


	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}
	
}