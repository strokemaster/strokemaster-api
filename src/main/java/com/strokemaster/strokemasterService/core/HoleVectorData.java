package com.strokemaster.strokemasterService.core;

import java.awt.geom.Path2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Class that contains a golf hole's vector data
 * 
 * @author Greg Morano
 *
 */
public class HoleVectorData {

	private ArrayList<Point2D> green;
	private ArrayList<Point2D> fairway;
	private HashMap<Integer, ArrayList<Point2D>> teeboxes;
	private ArrayList<Point2D> greenCenter;
	private ArrayList<Point2D> centralPath;
	private HashMap<Integer, ArrayList<Point2D>> teeboxCenters;
	private ArrayList<Point2D> perimeter;
	private HashMap<Integer, Point2D> trees;
	private HashMap<Integer, ArrayList<Point2D>> water;
	private HashMap<Integer, ArrayList<Point2D>> bunkers;
	private HashMap<Integer, ArrayList<Point2D>> cartpath;
	
	
	
	/**
	 * takes the JSON data from iGolf and parses it into data for each hole
	 * @param data
	 * @throws JSONException
	 */
	public HoleVectorData(JSONObject data, int holeNum) throws JSONException{

		JSONObject hole = data.getJSONObject("Holes").getJSONArray("Hole").getJSONObject(holeNum);
		
		
		this.green = getObjectPoints(hole.getJSONObject("Green"), 0);
		try{	
			this.fairway = getObjectPoints(hole.getJSONObject("Fairway") , 0);
		}catch(Exception e){
			System.out.println("Couldn't find any objects of type Fairway");
		}
		this.teeboxes = new HashMap<Integer, ArrayList<Point2D>>();
		for(int i = 0; i < hole.getJSONObject("Teebox").getInt("ShapeCount"); i++){
			this.teeboxes.put(i, getObjectPoints(hole.getJSONObject("Teebox"), i));
		}

		this.greenCenter = getObjectPoints(hole.getJSONObject("Greencenter"), 0);
		this.centralPath = getObjectPoints(hole.getJSONObject("Centralpath"), 0);
		
		this.teeboxCenters = new HashMap<Integer, ArrayList<Point2D>>();
		for(int i = 0; i < hole.getJSONObject("Teeboxcenter").getInt("ShapeCount"); i++){
			this.teeboxCenters.put(i,getObjectPoints(hole.getJSONObject("Teeboxcenter"), i));
		}

		this.perimeter = getObjectPoints(hole.getJSONObject("Perimeter"), 0);
		
		try{
			this.water = new HashMap<Integer, ArrayList<Point2D>>();
			for(int i = 0; i < hole.getJSONObject("Water").getInt("ShapeCount"); i++){
				//temp.add(getObjectPoints(data.getJSONObject("Water"), i));
				this.water.put(i,getObjectPoints(hole.getJSONObject("Water"), i));
			}
		}catch(Exception e){
			System.out.println("Couldn't find any objects of type Water");
		}
		
		try{
			this.bunkers = new HashMap<Integer, ArrayList<Point2D>>();
			for(int i = 0; i < hole.getJSONObject("Bunker").getInt("ShapeCount"); i++){
				this.bunkers.put(i,getObjectPoints(hole.getJSONObject("Bunker"), i));
			}
		}catch(Exception e){
			System.out.println("Couldn't find any objects of type Bunker");
		}
		
		HashMap<Integer, ArrayList<Point2D>> courseObjects;
		Path2D path = createPerimeterPath();
		int count = 0;
		
		try{
			this.trees = new HashMap<Integer, Point2D>();
			for(int i = 0; i < data.getJSONObject("Tree").getInt("ShapeCount"); i++){
				
				JSONObject arr = null;
				
				arr = data.getJSONObject("Tree").getJSONObject("Shapes").getJSONArray("Shape").getJSONObject(i);
				
				String str = arr.getString("Points");			
				String[] Point2D = str.split(" ");
	
				Point2D tempPoint = new Point2D.Double(Double.parseDouble(Point2D[0]), Double.parseDouble(Point2D[1]) );
				if(path.contains(tempPoint)){
					this.trees.put(count,tempPoint );
					count++;
				}
			}
		}catch(Exception e){
			System.out.println("Couldn't find any objects of type Tree");
		
		}
		
		count = 0;
		try{
			addPointsToWaterMap(data.getJSONObject("Pond"), "Pond", path, count);
			count  = this.water.size();
		}catch (Exception e){
			System.out.println("No Pond Objects found");
		}
		try{
			addPointsToWaterMap(data.getJSONObject("Lake"), "Lake" , path, count);
			count  = this.water.size();
		}catch (Exception e){
			System.out.println("No Lake Objects found");
		}
		try{
			addPointsToWaterMap(data.getJSONObject("Creek"), "Creek", path, count);
		}catch (Exception e){
			System.out.println("No Creek Objects found");
		}
		
		this.cartpath = new HashMap<Integer, ArrayList<Point2D>>();
		count = 0;
		JSONObject objectData  = data.getJSONObject("Path");
		courseObjects = populateNonHoleArray(objectData, "Path");
		for(ArrayList<Point2D> arr :courseObjects.values()){
			for(Point2D point : arr){
				if(path.contains(point)){
					this.cartpath.put(count, arr);
					count++;
					break;
				}
			}
		}

	}
	
	/*
	 * Getters and setters
	 */
	public ArrayList<Point2D> getGreen() {
		return green;
	}
	public void setGreen(ArrayList<Point2D> green) {
		this.green = green;
	}
	public ArrayList<Point2D> getFairway() {
		return fairway;
	}
	public void setFairway(ArrayList<Point2D> fairway) {
		this.fairway = fairway;
	}
	public HashMap<Integer, ArrayList<Point2D>> getTeeboxes() {
		return teeboxes;
	}
	public void setTeeboxes(HashMap<Integer, ArrayList<Point2D>> teeboxes) {
		this.teeboxes = teeboxes;
	}
	public ArrayList<Point2D> getGreenCenter() {
		return greenCenter;
	}
	public void setGreenCenter(ArrayList<Point2D> greenCenter) {
		this.greenCenter = greenCenter;
	}
	public ArrayList<Point2D> getCentralPath() {
		return centralPath;
	}
	public void setCentralPath(ArrayList<Point2D> centralPath) {
		this.centralPath = centralPath;
	}
	public HashMap<Integer, ArrayList<Point2D>> getTeeboxCenters() {
		return teeboxCenters;
	}
	public void setTeeboxCenters(HashMap<Integer, ArrayList<Point2D>> teeboxCenters) {
		this.teeboxCenters = teeboxCenters;
	}
	public HashMap<Integer, ArrayList<Point2D>> getBunkers() {
		return bunkers;
	}
	public void setBunkers(HashMap<Integer, ArrayList<Point2D>> bunkers) {
		this.bunkers = bunkers;
	}
	public ArrayList<Point2D> getPerimeter() {
		return perimeter;
	}
	public void setPerimeter(ArrayList<Point2D> perimeter) {
		this.perimeter = perimeter;
	}
	public HashMap<Integer, Point2D> getTrees() {
		return trees;
	}
	public void setTrees(HashMap<Integer, Point2D> trees) {
		this.trees = trees;
	}
	public HashMap<Integer, ArrayList<Point2D>> getWater() {
		return water;
	}
	public void setWater(HashMap<Integer, ArrayList<Point2D>> water) {
		this.water = water;
	}
	public HashMap<Integer, ArrayList<Point2D>> getCartpath() {
		return cartpath;
	}
	public void setCartpath(HashMap<Integer, ArrayList<Point2D>> cartpath) {
		this.cartpath = cartpath;
	}
	
	/**
	 * gets the points for the green from the JSON of a hole
	 * @param green
	 * @return
	 */
	public ArrayList<Point2D> getObjectPoints(JSONObject object, int shapeNum){
		ArrayList<Point2D> points = new ArrayList<Point2D>();
		JSONObject arr = null;
		
		try{
			arr = object.getJSONObject("Shapes").getJSONArray("Shape").getJSONObject(shapeNum);
			
			String str = arr.getString("Points");			
			String[] xyPoint2D = str.split(",");
			for(int i = 0; i < xyPoint2D.length; i++){
				String[] Point2D = xyPoint2D[i].split(" ");
				points.add(new Point2D.Double(Double.parseDouble(Point2D[0]), Double.parseDouble(Point2D[1]) ));
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return points;
	}
	
	
	public HashMap<Integer, ArrayList<Point2D>> populateNonHoleArray(JSONObject data, String name){
		
		HashMap<Integer, ArrayList<Point2D>> courseObjects = new HashMap<Integer, ArrayList<Point2D>>();
		try{		
			for(int i = 0; i < data.getInt("ShapeCount"); i++){
				courseObjects.put(i,getObjectPoints(data, i));
			}
		}catch(Exception e){
			System.out.println("Couldn't find any objects of type " + name);
		}
		return courseObjects;
	}
	
	public Path2D createPerimeterPath(){
		/*Path2D path = new Path2D.Double();
		path.moveTo(this.perimeter.get(0).getX(), this.perimeter.get(0).getY());
		for(Point2D point : this.perimeter){
			path.lineTo(point.getX(), point.getY());
		}
		path.lineTo(this.perimeter.get(0).getX(), this.perimeter.get(0).getY());
		path.closePath();
		return path;*/
		
		Path2D path = new Path2D.Double();
		double minx = this.perimeter.get(0).getX();
		double maxx = this.perimeter.get(0).getX();
		double miny = this.perimeter.get(0).getY();
		double maxy = this.perimeter.get(0).getY();
		
		for(Point2D point : this.perimeter){
			if(point.getX() > maxx){
				maxx = point.getX();
			}
			else if(point.getX() < minx){
				minx = point.getX();
			}
			if(point.getY() > maxy){
				maxy = point.getY();
			}
			else if(point.getY() < miny){
				miny = point.getY();
			}
		}
		path.moveTo(minx, miny);
		path.lineTo(minx, maxy);
		path.lineTo(maxx, maxy);
		path.lineTo(maxx, miny);
		path.lineTo(minx, miny);
		path.closePath();
		return path;
	}
	
	public void addPointsToWaterMap(JSONObject objectData, String type, Path2D path, int count){
		
		HashMap<Integer, ArrayList<Point2D>> courseObjects;
				
		courseObjects = populateNonHoleArray(objectData, type);
		
		for(ArrayList<Point2D> arr :courseObjects.values()){
			for(Point2D point : arr){
				if(path.contains(point)){
					this.water.put(count, arr);
					count++;
					break;
				}
			}
		}
		
	}
	
	
	//Polygon point test method not working using Path2D library class instead
	/**
	 * Determine if a point is inside a polygon object
	 * @param points
	 * @param point
	 * @return
	 *//*
	public static boolean isPointInPolygon(ArrayList<Point2D> points, Point2D point){
		double minx = points.get(0).getX();
		double miny = points.get(0).getY();
		double maxx = points.get(0).getX();
		double maxy = points.get(0).getY();
		boolean inPolygon = false;
		
		//Bounding box.  Find min and max points and create a bounding box 
		//determine quickly if the point may be in the area
		for(int i = 1; i < points.size(); i++){
			minx = Math.min(points.get(i).getX(), minx);
			miny = Math.min(points.get(i).getY(), miny);
			maxx = Math.max(points.get(i).getX(), maxx);
			maxy = Math.max(points.get(i).getY(), maxy);	
		}
		if ( point.getX() < minx || point.getX() > maxx || point.getY() < miny || point.getY() > maxy ){
            return false;
        }

		Polygon p = null;
        for ( int i = 0, j = points.size() - 1 ; i < points.size() ; j = i++ ){
            if ( ( points.get(i).getY() > point.getY() ) != ( points.get(j).getY() > point.getY() ) &&
                 point.getX() < ( points.get(j).getX() - points.get(i).getX() ) * ( point.getY() - points.get(i).getY() ) / 
                 ( points.get(j).getY() - points.get(i).getY() ) + points.get(i).getX() ){
            	
                inPolygon = !inPolygon;
            }
        }
		
		return inPolygon;
	}*/
	
}