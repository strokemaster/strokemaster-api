package com.strokemaster.strokemasterService.core;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;


/**
 * This class handles calls to igolf and creating the request urls
 * 
 * @author Greg Morano
 *
 */
public class IGolf{
	
	private static final String API_KEY = "HGdXeJAKOexWUyo";
	private static final String API_KEY_USERNAME = "api-connect.igolf.com";
	private static final String API_VERSION = "1.1";
	private static final String SIGNATURE_VERSION = "2.0";
	private static final String SIGNATURE_METHOD = "HmacSHA256";
	private static final String TIMESTAMP_RESPONSE_TYPE = "JSON";
	private static final String SECRET_KEY = "rybAa3uAd6omYOJbEbUdLPBpGGmFVY";
	
	/**
	 * This class creates the iGolf url
	 * @return iGolf url to use
	 */
	public static String getIGolfUrl(String actionCode){
		String signature = "";
		
		DateFormat sdf = new SimpleDateFormat("yyMMddHHmmssZZZZZ");
        String timestampStr = sdf.format(new Date());
        //
        String toSignStr = toSign(actionCode, API_KEY, API_KEY_USERNAME, API_VERSION, SIGNATURE_VERSION, SIGNATURE_METHOD, timestampStr, TIMESTAMP_RESPONSE_TYPE);
        try {
			signature = calculateSignature(toSignStr,SIGNATURE_METHOD, SECRET_KEY);
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        //takes the values and creates the url
        String url = "http://api-connect.igolf.com/rest/action/" + toUrl(actionCode, API_KEY, API_KEY_USERNAME, API_VERSION, SIGNATURE_VERSION, SIGNATURE_METHOD, timestampStr, TIMESTAMP_RESPONSE_TYPE, signature);
        return  url;
	}
	
	/**
	 * calulates the signature by creating a hash of the 
	 * toSignStr using the secretKey
	 * 
	 * @param toSignStr
	 * @param signMethod
	 * @param secretKey
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 * @throws UnsupportedEncodingException
	 */
	public static String calculateSignature(String toSignStr, String signMethod, String secretKey)
            throws NoSuchAlgorithmException, InvalidKeyException, UnsupportedEncodingException {
        
		Mac mac = Mac.getInstance(signMethod);

        mac.init(new SecretKeySpec(secretKey.getBytes("UTF-8"), mac.getAlgorithm()));
        byte[] rawSig = mac.doFinal(toSignStr.getBytes("UTF-8"));
        String s = Base64.encodeBase64URLSafeString(rawSig); // use Base64 with URL and Filename Safe Alphabet (RFC 4648 'base64url' encoding)
        return s;
    }
	
	/*
	 * Concatenate the url from the different parts
	 */
	public static String toUrl(String actionCode, String appApiKey, String apiUsername, String apiVersion, String sigVersion, String sigMethod, String timestampStr, String responseFormat, String sig) {
        StringBuilder builder = new StringBuilder();
        builder.append(actionCode)
            .append("/").append(appApiKey)
            .append("/").append(apiVersion)
            .append("/").append(sigVersion)
            .append("/").append(sigMethod)
            .append("/").append(sig)
            .append("/").append(timestampStr)
            .append("/").append(responseFormat);
        return builder.toString();
   }

	/**
	 * 
	 * Creates the toSignStr which is basically
	 * the normal url without the signature
	 * @return
	 */
    public static String toSign(String actionCode, String appApiKey, String apiUsername, String apiVersion, String sigVersion, String sigMethod, String timestampStr, String responseFormat) {
         StringBuilder builder = new StringBuilder();
         builder.append(actionCode)
             .append("/").append(appApiKey)
             .append("/").append(apiVersion)
             .append("/").append(sigVersion)
             .append("/").append(sigMethod)
             .append("/").append(timestampStr)
             .append("/").append(responseFormat);
         return builder.toString();
    }
}