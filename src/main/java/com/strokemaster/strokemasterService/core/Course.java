package com.strokemaster.strokemasterService.core;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Class that represents a golf course from iGolf data
 * 
 * @author Greg Morano
 *
 */
public class Course {

    private int active;
    private String address1;
    private String address2;
    private String city;
    private String classification;
    private double conditionRating;
    private int id_country;
    private String countryFull;
    private String countryShort;  
    private String id_course;
    //private int id_CourseGrouping;  //internal field
    private String courseName;
    private double distance;
    private String email; 
    //private int facilityid; //internal field
    private double latitude;
    private double longitude;
    private int scorecardAvailable;
    private int gpsAvailable;
    private String otherState;
    private double recommendRating;
    //private int id_state;  //shouldnt need
    private String stateFull;  
    private String stateShort;
    //private int syncOutputAvailable;  //internal field
    private String thumbnailImage;
    private String zipcode;
    
    /**
     * default constructor
     */
    public Course() {
        // Jackson deserialization
    }
    
    /**
     * Constructor for course object.  Takes a JSONObject of Course 
     * info and creates an object.
     * 
     * @param list
     */
    public Course(JSONObject list) {
       try {
			this.active = list.getInt("active");
			this.address1 = list.getString("address1");
			if(list.has("address2")){
				this.address2 = list.getString("address2");
			}
			this.city = list.getString("city");
			this.classification = list.getString("classification");
			this.conditionRating = list.getDouble("conditionRating");
			this.id_country = list.getInt("id_country");
			this.countryFull = list.getString("countryFull");
			this.countryShort = list.getString("countryShort");
			this.id_course = list.getString("id_course");
			//this.id_CourseGrouping = list.getInt("id_CourseGrouping");
			this.courseName = list.getString("courseName");
			//distance should be returned if you search based on a location
			if(list.has("distance")){
				this.distance = list.getDouble("distance");
			}
			if(list.has("email")){
				this.email = list.getString("email");
			}
			this.latitude = list.getDouble("latitude");
			this.longitude = list.getDouble("longitude");
			this.scorecardAvailable = list.getInt("scorecardAvailable");
			this.gpsAvailable = list.getInt("gpsAvailable");
			if(list.has("otherState")){
				this.otherState = list.getString("otherState");
			}
			this.recommendRating = list.getDouble("recommendRating");
			//this.id_state = list.getInt("id_state");
			this.stateFull = list.getString("stateFull");
			this.stateShort = list.getString("stateShort");
			if(list.has("thumbnailImage")){
				this.thumbnailImage = list.getString("thumbnailImage");
			}
			if(list.has("zipcode")){
				this.zipcode = list.getString("zipcode");
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       
    }
    
    /**
     *Getters for Course class 
     * @return
     */
	public int getActive() {
		return active;
	}

	public String getAddress1() {
		return address1;
	}

	public String getAddress2() {
		return address2;
	}

	public String getCity() {
		return city;
	}

	public String getClassification() {
		return classification;
	}

	public double getConditionRating() {
		return conditionRating;
	}

	public int getId_country() {
		return id_country;
	}

	public String getCountryShort() {
		return countryShort;
	}
	
	public String getCountryFull() {
		return countryFull;
	}

	public String getId_course() {
		return id_course;
	}

	public String getCourseName() {
		return courseName;
	}

	public double getDistance() {
		return distance;
	}

	public String getEmail() {
		return email;
	}

	public double getLatitude() {
		return latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public int getScorecardAvailable() {
		return scorecardAvailable;
	}

	public int getGpsAvailable() {
		return gpsAvailable;
	}

	public String getOtherState() {
		return otherState;
	}

	public double getRecommendRating() {
		return recommendRating;
	}

	public String getStateShort() {
		return stateShort;
	}
	
	public String getStateFull() {
		return stateFull;
	}

	public String getThumbnailImage() {
		return thumbnailImage;
	}

	public String getZipcode() {
		return zipcode;
	}

	
    
    
}