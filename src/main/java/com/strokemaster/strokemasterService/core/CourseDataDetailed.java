package com.strokemaster.strokemasterService.core;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.json.JSONException;
import org.json.JSONObject;

public class CourseDataDetailed extends CourseData{
    /*private int active;
    private String address1;
    private String address2;
    private String city;
    private String classType;  //had to change name since class is a reserved word
    private String countryFull;
    private String countryShort;
    private String courseName;
    private String dressCode;
    private String email;
    private int gpsAvailable;
    private String image;
    private int layoutHoles;
    private int layoutTotalHoles;
    private String layoutName;
    private String otherState;
    private String phone;
    private int seasonal;
    private String stateFull;
    private String stateShort;
    private String thumbnailImage;
    private String url;  //course website
    private String zipcode;*/
	private Timestamp courseCreated;
	private Timestamp courseDeactivated;
	private Timestamp courseModified;
	private String dayClosed;
	private String directorName;
	private String fax;
	private int fivesome;
	private Timestamp gpsModified;
	private Double weekend9;
	private Double weekend18;
	private Double weekday9;
	private Double weekday18;
	private Double twilight;
	private String professionalName;
	private String proShopOpen;
	private String proShopClose;
	private int  scorecardAvailable;
	private Timestamp scorecardModified;
	private int seasonEnd;
	private int seasonStart;
	private String superintendentName;
	private int vectorAvailable;
	private Timestamp vectorModified;
    
    /**
     * default constructor
     */
    public CourseDataDetailed() {
        // Jackson deserialization
    }

    /**
     * course details object that holds details of a 
     * specific golf course
     * 
     * @param course
     */
    public CourseDataDetailed(JSONObject course) {
    	super(course);
    	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-ddhh:mm:ss");
    	//SimpleDateFormat timeFormatter = new SimpleDateFormat("hh:mm:ss");
    	
    	try {
    		if(course.has("courseCreated")){
    			this.courseCreated =  new Timestamp(formatter.parse(course.getString("courseCreated")).getTime());
    		}
    		if(course.has("courseDeactivated")){
    			this.courseDeactivated =  new Timestamp(formatter.parse(course.getString("courseDeactivated")).getTime());
    		}
    		if(course.has("courseModified")){
    			this.courseModified =  new Timestamp(formatter.parse(course.getString("courseModified")).getTime());
    		}
    		if(course.has("dayClosed")){
    			this.dayClosed = course.getString("dayClosed");
    		}
    		this.directorName = course.getString("directorName");
    		this.fax = course.getString("fax");
    		this.fivesome = course.getInt("fivesome");
    		if(course.has("gpsModified")){
    			this.gpsModified =  new Timestamp(formatter.parse(course.getString("gpsModified")).getTime());
    		}
    		
    		this.weekend9 = course.getDouble("weekend9");
    		this.weekend18 = course.getDouble("weekend18");
    		this.weekday9 = course.getDouble("weekday9");
    		this.weekday18 = course.getDouble("weekday18");
    		this.twilight = course.getDouble("twilight");


    		this.professionalName = course.getString("professionalName");
    		if(course.has("proShopOpen")){
    			//this.proShopOpen =  new Timestamp(timeFormatter.parse(course.getString("proShopOpen")).getTime());
    			this.proShopOpen = course.getString("proShopOpen");
    		}
    		if(course.has("proShopClose")){
    			//this.proShopClose =  new Timestamp(timeFormatter.parse(course.getString("proShopClose")).getTime());
    			this.proShopClose = course.getString("proShopClose");
    		}
    		this.scorecardAvailable = course.getInt("scorecardAvailable");
    		if(course.has("scorecardModified")){
    			this.scorecardModified =  new Timestamp(formatter.parse(course.getString("scorecardModified")).getTime());
    		}
    		this.seasonEnd = course.getInt("seasonEnd");
    		this.seasonStart = course.getInt("seasonStart");
    		this.superintendentName = course.getString("superintendentName");
    		this.vectorAvailable = course.getInt("vectorAvailable");
    		if(course.has("vectorModified")){
    			this.vectorModified =  new Timestamp(formatter.parse(course.getString("vectorModified")).getTime());
    		}
    		
    		
    	} catch (JSONException | ParseException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    }
    
    /**
     * getters
     * @return
     */
    public Timestamp getCourseCreated() {
		return courseCreated;
	}

	public Timestamp getCourseDeactivated() {
		return courseDeactivated;
	}

	public Timestamp getCourseModified() {
		return courseModified;
	}

	public String getDayClosed() {
		return dayClosed;
	}

	public String getDirectorName() {
		return directorName;
	}

	public String getFax() {
		return fax;
	}

	public int getFivesome() {
		return fivesome;
	}

	public Timestamp getGpsModified() {
		return gpsModified;
	}

	public Double getWeekend9() {
		return weekend9;
	}

	public Double getWeekend18() {
		return weekend18;
	}

	public Double getWeekday9() {
		return weekday9;
	}

	public Double getWeekday18() {
		return weekday18;
	}

	public Double getTwilight() {
		return twilight;
	}

	public String getProfessionalName() {
		return professionalName;
	}

	public String getProShopOpen() {
		return proShopOpen;
	}

	public String getProShopClose() {
		return proShopClose;
	}

	public int getScorecardAvailable() {
		return scorecardAvailable;
	}

	public Timestamp getScorecardModified() {
		return scorecardModified;
	}

	public int getSeasonEnd() {
		return seasonEnd;
	}

	public int getSeasonStart() {
		return seasonStart;
	}

	public String getSuperintendentName() {
		return superintendentName;
	}

	public int getVectorAvailable() {
		return vectorAvailable;
	}

	public Timestamp getVectorModified() {
		return vectorModified;
	}
    
    
    
}