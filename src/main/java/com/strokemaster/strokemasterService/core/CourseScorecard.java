/**
 * @author Greg Morano
 */

package com.strokemaster.strokemasterService.core;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CourseScorecard {
	
	private String courseId;
	private String courseName;
	private ArrayList<Integer> mensHandicap;
	private ArrayList<Integer> mensPar;
	private int mensParIn;
	private int mensParOut;
	private int mensParTotal;
	private ArrayList<Integer> ladiesHandicap;
	private ArrayList<Integer> ladiesPar;
	private int ladiesParIn;
	private int ladiesParOut;
	private int ladiesParTotal;
	
	public CourseScorecard(JSONObject course) {
    	try {
    		if(course.has("id_course")){
    			this.courseId = course.getString("id_course");
    		}
    		if(course.has("courseName")){
    			this.courseName = course.getString("courseName");
    		}
    		JSONArray tempArr = null;
    		if(course.has("menScorecardList")){
    			tempArr = course.getJSONArray("menScorecardList").getJSONObject(0).getJSONArray("hcpHole");
    			mensHandicap = new ArrayList<Integer>();
    			for(int i = 0; i < tempArr.length(); i++){
    				mensHandicap.add(tempArr.getInt(i));
    			}
    		}
    		
    		if(course.has("menScorecardList")){
    			tempArr = course.getJSONArray("menScorecardList").getJSONObject(0).getJSONArray("parHole");
    			mensPar = new ArrayList<Integer>();
    			for(int i = 0; i < tempArr.length(); i++){
    				mensPar.add(tempArr.getInt(i));
    			}
    		}
    		
    		if(course.has("menScorecardList")){
    			mensParOut = course.getJSONArray("menScorecardList").getJSONObject(0).getInt("parOut");
    		}
    		
    		if(course.has("menScorecardList")){
    			mensParIn = course.getJSONArray("menScorecardList").getJSONObject(0).getInt("parIn");
    		}
    		
    		if(course.has("menScorecardList")){
    			mensParTotal = course.getJSONArray("menScorecardList").getJSONObject(0).getInt("parTotal");
    		}
    		
    		if(course.has("wmnScorecardList")){
    			tempArr = course.getJSONArray("wmnScorecardList").getJSONObject(0).getJSONArray("hcpHole");
    			ladiesHandicap = new ArrayList<Integer>();
    			for(int i = 0; i < tempArr.length(); i++){
    				ladiesHandicap.add(tempArr.getInt(i));
    			}
    		}
    		
    		if(course.has("wmnScorecardList")){
    			tempArr = course.getJSONArray("wmnScorecardList").getJSONObject(0).getJSONArray("parHole");
    			ladiesPar = new ArrayList<Integer>();
    			for(int i = 0; i < tempArr.length(); i++){
    				ladiesPar.add(tempArr.getInt(i));
    			}
    		}
    		
    		if(course.has("wmnScorecardList")){
    			ladiesParOut = course.getJSONArray("wmnScorecardList").getJSONObject(0).getInt("parOut");
    		}
    		
    		if(course.has("wmnScorecardList")){
    			ladiesParIn = course.getJSONArray("wmnScorecardList").getJSONObject(0).getInt("parIn");
    		}
    		
    		if(course.has("wmnScorecardList")){
    			ladiesParTotal = course.getJSONArray("wmnScorecardList").getJSONObject(0).getInt("parTotal");
    		}
    	}catch (JSONException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    		System.err.println("Failed creating course scorecard");
    	}
	}
	
	
	public String getCourseId() {
		return courseId;
	}
	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public ArrayList<Integer> getMensHandicap() {
		return mensHandicap;
	}
	public void setMensHandicap(ArrayList<Integer> mensHandicap) {
		this.mensHandicap = mensHandicap;
	}
	public ArrayList<Integer> getMensPar() {
		return mensPar;
	}
	public void setMensPar(ArrayList<Integer> mensPar) {
		this.mensPar = mensPar;
	}
	public int getMensParIn() {
		return mensParIn;
	}
	public void setMensParIn(int mensParIn) {
		this.mensParIn = mensParIn;
	}
	public int getMensParOut() {
		return mensParOut;
	}
	public void setMensParOut(int mensParOut) {
		this.mensParOut = mensParOut;
	}
	public int getMensParTotal() {
		return mensParTotal;
	}
	public void setMensParTotal(int mensParTotal) {
		this.mensParTotal = mensParTotal;
	}
	public ArrayList<Integer> getLadiesHandicap() {
		return ladiesHandicap;
	}
	public void setLadiesHandicap(ArrayList<Integer> ladiesHandicap) {
		this.ladiesHandicap = ladiesHandicap;
	}
	public ArrayList<Integer> getLadiesPar() {
		return ladiesPar;
	}
	public void setLadiesPar(ArrayList<Integer> ladiesPar) {
		this.ladiesPar = ladiesPar;
	}
	public int getLadiesParIn() {
		return ladiesParIn;
	}
	public void setLadiesParIn(int ladiesParIn) {
		this.ladiesParIn = ladiesParIn;
	}
	public int getLadiesParOut() {
		return ladiesParOut;
	}
	public void setLadiesParOut(int ladiesParOut) {
		this.ladiesParOut = ladiesParOut;
	}
	public int getLadiesParTotal() {
		return ladiesParTotal;
	}
	public void setLadiesParTotal(int ladiesParTotal) {
		this.ladiesParTotal = ladiesParTotal;
	}
	
	
	
	
}