
package com.strokemaster.strokemasterService.core;


/**
 * Object that represents data from a given hole
 * @author Greg Morano
 *
 */
public class ClubStats {
	
	private String clubName;
	private double avgDist;
	private int min;
	private int max;

	public ClubStats(String club, double avg, int min, int max){
		this.avgDist = avg;
		this.clubName = club;
		this.min = min;
		this.max = max;
	}

	/*
	 * getters and setter
	 */
	
	public double getAvgDist() {
		return avgDist;
	}


	public void setAvgDist(double avgDist) {
		this.avgDist = avgDist;
	}

	public String getClubName() {
		return clubName;
	}

	public void setClubName(String clubName) {
		this.clubName = clubName;
	}

	public int getMin() {
		return min;
	}

	public void setMin(int min) {
		this.min = min;
	}

	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		this.max = max;
	}



	
}