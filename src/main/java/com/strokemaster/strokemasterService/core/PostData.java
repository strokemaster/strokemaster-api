package com.strokemaster.strokemasterService.core;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties
public class PostData {
	@JsonProperty("Data")
	//private JSONObject data;
    private ArrayList<User> data;

    
    /**
     * default constructor
     */
    public PostData() {
        // Jackson deserialization
    }
    
    
    public PostData(ArrayList<User> res){
    	this.data = res;
    }

	public ArrayList<User> getData() {
		return data;
	}

	public void setData(ArrayList<User> data) {
		this.data = data;
	}
	
    /*
    public PostData(JSONObject res){
    	this.data = res;
    }

	public JSONObject getData() {
		return data;
	}

	public void setData(JSONObject data) {
		this.data = data;
	}*/

    
}