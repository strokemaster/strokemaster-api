
/**
 * @author Greg Morano
 */

package com.strokemaster.strokemasterService.core;

import org.json.JSONException;
import org.json.JSONObject;

public class TeeDetails {
	


	private double ratingMen;
	private double ratingWomen;
	private int slopeMen;
	private int slopeWomen;	
	private String teeName;
	private String teeColorName;
	private String teeColorValue;
	private int ydsTotal;
	private String yds1to9;		
	private String yds10to18;	
	private String yds1to18;	
	private String ydsHole;
	
	
	public TeeDetails(JSONObject course) {
    	try {
    		
    			   			
    		if(course.has("ratingMen")){
    			this.ratingMen = course.getDouble("ratingMen");
    		}
    		if(course.has("ratingWomen")){
    			this.ratingWomen = course.getDouble("ratingWomen");
    		}
    		if(course.has("slopeMen")){
    			this.slopeMen = course.getInt("slopeMen");
    		}
    		if(course.has("slopeWomen")){
    			this.slopeWomen = course.getInt("slopeWomen");
    		}
    		if(course.has("teeName")){
    			this.teeName = course.getString("teeName");
    		}
    		if(course.has("teeColorName")){
    			this.teeColorName = course.getString("teeColorName");
    		}
    		if(course.has("teeColorValue")){
    			this.teeColorValue = course.getString("teeColorValue");
    		}
    		if(course.has("ydsTotal")){
    			this.ydsTotal = course.getInt("ydsTotal");
    		}
    		if(course.has("yds1to9")){
    			this.yds1to9 = course.getString("yds1to9");
    		}
    		if(course.has("yds10to18")){
    			this.yds10to18 = course.getString("yds10to18");
    		}
    		if(course.has("yds1to18")){
    			this.yds1to18 = course.getString("yds1to18");
    		}
    		if(course.has("ydsHole")){
    			this.ydsHole = course.getString("ydsHole");
    		}
    		
    		
    	}catch (JSONException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    		System.err.println("Failed creating course tee details");
    	}
	}
	
	public double getRatingMen() {
		return ratingMen;
	}
	public void setRatingMen(double ratingMen) {
		this.ratingMen = ratingMen;
	}
	public double getRatingWomen() {
		return ratingWomen;
	}
	public void setRatingWomen(double ratingWomen) {
		this.ratingWomen = ratingWomen;
	}
	public int getSlopeMen() {
		return slopeMen;
	}
	public void setSlopeMen(int slopeMen) {
		this.slopeMen = slopeMen;
	}
	public int getSlopeWomen() {
		return slopeWomen;
	}
	public void setSlopeWomen(int slopeWomen) {
		this.slopeWomen = slopeWomen;
	}
	public String getTeeName() {
		return teeName;
	}
	public void setTeeName(String teeName) {
		this.teeName = teeName;
	}
	public String getTeeColorName() {
		return teeColorName;
	}
	public void setTeeColorName(String teeColorName) {
		this.teeColorName = teeColorName;
	}
	public String getTeeColorValue() {
		return teeColorValue;
	}
	public void setTeeColorValue(String teeColorValue) {
		this.teeColorValue = teeColorValue;
	}
	public int getYdsTotal() {
		return ydsTotal;
	}
	public void setYdsTotal(int ydsTotal) {
		this.ydsTotal = ydsTotal;
	}
	public String getYds1to9() {
		return yds1to9;
	}
	public void setYds1to9(String yds1to9) {
		this.yds1to9 = yds1to9;
	}
	public String getYds10to18() {
		return yds10to18;
	}
	public void setYds10to18(String yds10to18) {
		this.yds10to18 = yds10to18;
	}
	public String getYds1to18() {
		return yds1to18;
	}
	public void setYds1to18(String yds1to18) {
		this.yds1to18 = yds1to18;
	}
	public String getYdsHole() {
		return ydsHole;
	}
	public void setYdsHole(String ydsHole) {
		this.ydsHole = ydsHole;
	}


}

