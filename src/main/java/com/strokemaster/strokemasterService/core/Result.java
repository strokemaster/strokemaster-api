package com.strokemaster.strokemasterService.core;

import com.fasterxml.jackson.annotation.JsonProperty;


public class Result {
	@JsonProperty("Success")
    private int success;

    
    /**
     * default constructor
     */
    public Result() {
        // Jackson deserialization
    }
    
    public Result(int res){
    	this.success = res;
    }

	public int getSuccess() {
		return success;
	}

	public void setSuccess(int success) {
		this.success = success;
	}
 
    
}