package com.strokemaster.strokemasterService.core;

import org.json.JSONException;
import org.json.JSONObject;

public class CourseData {
    private int active;
    private String address1;
    private String address2;
    private String city;
    private String classType;  //had to change name since class is a reserved word
    private String countryFull;
    private String countryShort;
    private String courseName;
    private String dressCode;
    private String email;
    private int gpsAvailable;
    private String image;
    private int layoutHoles;
    private int layoutTotalHoles;
    private String layoutName;
    private String otherState;
    private String phone;
    private int seasonal;
    private String stateFull;
    private String stateShort;
    private String thumbnailImage;
    private String url;  //course website
    private String zipcode;
    
    /**
     * default constructor
     */
    public CourseData() {
        // Jackson deserialization
    }

    /**
     * course details object that holds details of a 
     * specific golf course
     * 
     * @param course
     */
    public CourseData(JSONObject course) {
    	try {
    		if(course.has("active")){
    			this.active = course.getInt("active");
    		}
    		this.address1 = course.getString("address1");
    		if(course.has("address2")){
    			this.address2 = course.getString("address2");
    		}
    		this.city = course.getString("city");
    		this.classType = course.getString("class");
    		if(course.has("countryFull")){
    			this.countryFull = course.getString("countryFull");
    		}
    		if(course.has("countryShort")){
    			this.countryShort = course.getString("countryShort");
    		}
    		this.courseName = course.getString("courseName");
    		this.dressCode = course.getString("dressCode");
    		if(course.has("email")){
    			this.email = course.getString("email");
    		}
    		this.gpsAvailable = course.getInt("gpsAvailable");
    		if(course.has("image")){
    			this.image = course.getString("image");
    		}
    		this.layoutHoles = course.getInt("layoutHoles");
    		this.layoutName = course.getString("layoutName");
    		this.layoutTotalHoles = course.getInt("layoutTotalHoles");
    		if(course.has("otherState")){
    			this.otherState = course.getString("otherState");
    		}
    		this.phone = course.getString("phone");
    		this.seasonal = course.getInt("seasonal");
    		if(course.has("stateFull")){
    			this.stateFull = course.getString("stateFull");
    		}
    		if(course.has("stateShort")){
    			this.stateShort = course.getString("stateShort");
    		}
    		if(course.has("thumbnailImage")){
    			this.thumbnailImage = course.getString("thumbnailImage");
    		}
    		this.url = course.getString("url");
    		if(course.has("zipcode")){
    			this.zipcode = course.getString("zipcode");
    		}
    	} catch (JSONException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    }

    /**
     * getters
     * @return
     */
	public int getActive() {
		return active;
	}

	public String getAddress1() {
		return address1;
	}

	public String getAddress2() {
		return address2;
	}

	public String getCity() {
		return city;
	}

	public String getClassType() {
		return classType;
	}

	public String getCountryFull() {
		return countryFull;
	}
	
	public String getCountryShort() {
		return countryShort;
	}

	public String getCourseName() {
		return courseName;
	}

	public String getDressCode() {
		return dressCode;
	}

	public String getEmail() {
		return email;
	}

	public int getGpsAvailable() {
		return gpsAvailable;
	}

	public String getImage() {
		return image;
	}

	public int getLayoutHoles() {
		return layoutHoles;
	}

	public int getLayoutTotalHoles() {
		return layoutTotalHoles;
	}

	public String getLayoutName() {
		return layoutName;
	}
	
	public String getOtherState() {
		return otherState;
	}


	public String getPhone() {
		return phone;
	}

	public int getSeasonal() {
		return seasonal;
	}

	public String getStateFull() {
		return stateFull;
	}
	
	public String getStateShort() {
		return stateShort;
	}

	public String getThumbnailImage() {
		return thumbnailImage;
	}

	public String getUrl() {
		return url;
	}

	public String getZipcode() {
		return zipcode;
	}
    
    
}