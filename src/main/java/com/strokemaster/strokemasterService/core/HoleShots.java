
package com.strokemaster.strokemasterService.core;


import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Object that represents data from a given hole
 * @author Greg Morano
 *
 */
public class HoleShots {
	
	@JsonProperty("Yardages")
	private int[] yardages;
	@JsonProperty("Clubs")
	private String[] clubs;
	@JsonProperty("Terrain")
	private String[] terrain;
	@JsonProperty("Par")
	private int par;
	@JsonProperty("StartDist")
	private int[] startDist;
	@JsonProperty("EndDist")
	private int[] endDist;
	@JsonProperty("StartPoint")
	private String[] startPoint;
	@JsonProperty("EndPoint")
	private String[] endPoint;
	
	
	
	public HoleShots() {
		// TODO Auto-generated constructor stub
	}
	
	
	public HoleShots(int[] yardages, String[] clubs, String[] terrain, int par,
			int[] startDist, int[] endDist, String[] startPoint,
			String[] endPoint) {
		super();
		this.yardages = yardages;
		this.clubs = clubs;
		this.terrain = terrain;
		this.par = par;
		this.startDist = startDist;
		this.endDist = endDist;
		this.startPoint = startPoint;
		this.endPoint = endPoint;
	}


	/*
	 * Getters and Setters
	 * 
	 */
	public int[] getYardages() {
		return yardages;
	}
	public void setYardages(int[] yardages) {
		this.yardages = yardages;
	}
	public String[] getClubs() {
		return clubs;
	}
	public void setClubs(String[] clubs) {
		this.clubs = clubs;
	}
	public String[] getTerrain() {
		return terrain;
	}
	public void setTerrain(String[] terrain) {
		this.terrain = terrain;
	}
	public int getPar() {
		return par;
	}
	public void setPar(int par) {
		this.par = par;
	}


	public int[] getStartDist() {
		return startDist;
	}


	public void setStartDist(int[] startDist) {
		this.startDist = startDist;
	}


	public int[] getEndDist() {
		return endDist;
	}


	public void setEndDist(int[] endDist) {
		this.endDist = endDist;
	}


	public String[] getStartPoint() {
		return startPoint;
	}


	public void setStartPoint(String[] startPoint) {
		this.startPoint = startPoint;
	}


	public String[] getEndPoint() {
		return endPoint;
	}


	public void setEndPoint(String[] endPoint) {
		this.endPoint = endPoint;
	}

	
	
	
	
}