package com.strokemaster.strokemasterService.core;

import java.util.ArrayList;


/**
 * Class that contains a list of courses to return
 * to the IOS layer
 * 
 * @author Greg Morano
 *
 */
public class CourseList {

    private ArrayList<Course> courses;
    private int numCourses;

    /**
     * default constructor
     */
    public CourseList() {
        // Jackson deserialization
    }

    /**
     * populates the course list by making a course for each 
     * JSONObject that is given (each object is a course)
     * @param list
     * @param numCourses
     */
    public CourseList(ArrayList<Course> list, int numCourses) {
        this.courses = list;
        this.numCourses = numCourses;
    }

    /**
     * getters
     * 
     */
    public ArrayList<Course> getCourses() {
        return courses;
    }
    
    public int getNumCourses() {
    	return numCourses;
    }
}