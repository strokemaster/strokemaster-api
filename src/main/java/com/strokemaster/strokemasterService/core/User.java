package com.strokemaster.strokemasterService.core;

import com.fasterxml.jackson.annotation.JsonProperty;


public class User {
	@JsonProperty("Email")
    private String email;
	@JsonProperty("Password")
    private String password;

    
    /**
     * default constructor
     */
    public User() {
        // Jackson deserialization
    }
    
    public User(String email, String pass){
    	this.email = email;
    	this.password = pass;
    }

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
    
    
    
}