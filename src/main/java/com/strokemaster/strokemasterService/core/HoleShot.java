
package com.strokemaster.strokemasterService.core;

import org.json.JSONObject;

/**
 * Object that represents data from a given hole
 * @author Greg Morano
 *
 */
public class HoleShot {
	
	private int roundId;
	private String courseId;
	private int userId;
	private int holeNum;
	private int par;
	private int strokeNum;
	private int distance;
	private int terrainId;
	private int clubId;

	public HoleShot(JSONObject hole, int roundId, int userId, String courseId, int holeNum){
		this.roundId = roundId;
		this.userId = userId;
		this.courseId = courseId;
		this.holeNum = holeNum + 1;
		try{
			this.par = hole.getInt("Par");
			this.strokeNum = hole.getJSONArray("shots").length();
			this.distance = hole.getJSONArray("shots").getInt(holeNum);
			this.terrainId = hole.getJSONArray("terrain").getInt(holeNum);
			this.clubId = hole.getJSONArray("clubs").getInt(holeNum);
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}

	
	/*
	 * getters and setter
	 */

	public int getRoundId() {
		return roundId;
	}

	public void setRoundId(int roundId) {
		this.roundId = roundId;
	}

	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getHoleNum() {
		return holeNum;
	}

	public void setHoleNum(int holeNum) {
		this.holeNum = holeNum;
	}

	public int getStrokeNum() {
		return strokeNum;
	}

	public void setStrokeNum(int strokeNum) {
		this.strokeNum = strokeNum;
	}

	public int getDistance() {
		return distance;
	}

	public void setDistance(int distance) {
		this.distance = distance;
	}

	public int getTerrainId() {
		return terrainId;
	}

	public void setTerrainId(int terrainId) {
		this.terrainId = terrainId;
	}

	public int getClubId() {
		return clubId;
	}

	public void setClubId(int clubId) {
		this.clubId = clubId;
	}
	
	public int getPar() {
		return par;
	}

	public void setPar(int par) {
		this.par = par;
	}
	
	
	
}