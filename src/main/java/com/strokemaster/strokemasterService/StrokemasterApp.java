package com.strokemaster.strokemasterService;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

import com.strokemaster.strokemasterService.resources.CourseDataResource;
import com.strokemaster.strokemasterService.resources.CourseListResource;
import com.strokemaster.strokemasterService.resources.CourseScorecardListResource;
import com.strokemaster.strokemasterService.resources.CreateUserResource;
import com.strokemaster.strokemasterService.resources.EndRoundResource;
import com.strokemaster.strokemasterService.resources.LoginResource;
import com.strokemaster.strokemasterService.resources.LeaderboardResource;
import com.strokemaster.strokemasterService.resources.StatsHomeValuesResource;
import com.strokemaster.strokemasterService.resources.StrokemasterResource;
import com.strokemaster.strokemasterService.resources.VectorDataResource;
import com.strokemaster.strokemasterService.healthcheck.StrokemasterHealthcheck;
import com.strokemaster.strokemasterService.resources.*;


/**
 * Main class where everything else runs from
 * app is initialized here
 * @author Greg Morano
 *
 */
public class StrokemasterApp extends Application<StrokemasterConfig> {
	
	/**
	 * initialize the app and run it
	 * @param args
	 * @throws Exception
	 */
    public static void main(String[] args) throws Exception {
        new StrokemasterApp().run(args);
    }

    @Override
    public String getName() {
        return "strokemaster";
    }

    @Override
    public void initialize(Bootstrap<StrokemasterConfig> bootstrap) {
        // nothing to do yet
    }

    /**
     * creates and registers the resources so the server
     * knows what it's listening for
     */
    @Override
    public void run(StrokemasterConfig configuration,
                    Environment environment) {
    	/*
    	 * Resource Creation
    	 */
        final StrokemasterResource resource = new StrokemasterResource(
                configuration.getTemplate(),configuration.getDefaultName() );
        
        final CourseDataResource courseResource = new CourseDataResource(
                configuration.getTemplate() );
        
        final CourseListResource courseListResource = new CourseListResource(
                configuration.getTemplate() );
        final LoginResource loginResource = new LoginResource(configuration.getTemplate());
        final CreateUserResource createUserResource = new CreateUserResource(configuration.getTemplate());
        final VectorDataResource vectorDataResource = new VectorDataResource(configuration.getTemplate());
        final EndRoundResource endRoundResource = new EndRoundResource(configuration.getTemplate());
        final CourseScorecardListResource scoreCardListResource = new CourseScorecardListResource(configuration.getTemplate());
        final StatsHomeValuesResource statsHomeValuesResource = new StatsHomeValuesResource(configuration.getTemplate());
        final LeaderboardResource leaderboardResource = new LeaderboardResource(configuration.getTemplate());
        final DiagnosticStatsResource diagnosticStatsResource = new DiagnosticStatsResource(configuration.getTemplate());
        final GameSummaryResource gameSummaryResource = new GameSummaryResource(configuration.getTemplate());
        final ClubResource clubResource = new ClubResource(configuration.getTemplate());
        final CourseTeeDetailsResource teeResource = new CourseTeeDetailsResource(configuration.getTemplate());
        final ClubStatsResource clubStatsResource = new ClubStatsResource(configuration.getTemplate());

        /*
         * runs health checks
         */
        final StrokemasterHealthcheck healthCheck =
            new StrokemasterHealthcheck(configuration.getTemplate());
        
        /*
         * register resources
         */
        environment.healthChecks().register("template", healthCheck);
        environment.jersey().register(resource);
        environment.jersey().register(courseResource);
        environment.jersey().register(courseListResource);
        environment.jersey().register(loginResource);
        environment.jersey().register(createUserResource);
        environment.jersey().register(vectorDataResource);
        environment.jersey().register(scoreCardListResource);
        environment.jersey().register(endRoundResource);
        environment.jersey().register(statsHomeValuesResource);
        environment.jersey().register(leaderboardResource);
        environment.jersey().register(diagnosticStatsResource);
        environment.jersey().register(gameSummaryResource);
        environment.jersey().register(clubResource);
        environment.jersey().register(teeResource);
        environment.jersey().register(clubStatsResource);

    }

}