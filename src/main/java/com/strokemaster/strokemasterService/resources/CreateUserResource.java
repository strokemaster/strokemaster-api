package com.strokemaster.strokemasterService.resources;


import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.strokemaster.strokemasterService.core.Result;
import com.strokemaster.strokemasterService.core.User;

/**
 * Resource for creating new users
 * @author Greg Morano
 *
 */
@Path("/create-user")
@Produces(MediaType.APPLICATION_JSON)
public class CreateUserResource {
	

	@SuppressWarnings("unused")
	private final String template; 
    
    public CreateUserResource(String template) {
        this.template = template;
    }

    /**
     * Creates a user and returns result
     * @param email
     * @param password
     * @return
     */
    @GET
    @Timed
    public int getUser(@QueryParam("email") String email, @QueryParam("pass") String password) {
         	        		
		return createUser(email, password);
    }
    
    
    @POST
    @Timed
    @JsonIgnoreProperties(ignoreUnknown = true)
    @Consumes(MediaType.APPLICATION_JSON)
    public Result getUser(User myData) throws JsonParseException, JsonMappingException, IOException {
        
		return new Result(createUser(myData.getEmail(), myData.getPassword()));
		
    }
   
    /**
     * creates a user with the supplied values.  Adds the user to the database
     * @param email
     * @param pass
     * @return
     */
    public int createUser(String email, String pass){
    	
    	// The connection details for the mysql database
		String url = "jdbc:mysql://strokemasterdb-dev.czhvjbu7ieji.us-west-2.rds.amazonaws.com:3306/";
		String userName = "struckmaster";
		String password = "MasterBlaster";
		String dbName = "StrokemasterDBDev";
		String driver = "com.mysql.jdbc.Driver";
		
		int result = -1;
		// SQL Objects
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		
		try {
			//connect driver and open connection
			Class.forName(driver);
            conn = DriverManager.getConnection(url + dbName, userName, password);
            //create query and substitute values
            ps = conn.prepareStatement("INSERT INTO User(EMAIL, PASSWORD) VALUES(?,?)");
            ps.setString(1, email);
            ps.setString(2, pass);
            result = ps.executeUpdate();
            

        } catch (SQLException | ClassNotFoundException ex) {
            //Logger lgr = Logger.getLogger(Version.class.getName());
            //lgr.log(Level.SEVERE, ex.getMessage(), ex);
        	System.out.println("Error connecting to SQL or executing query");

        } finally {
        	//clean up any open connections
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }

            } catch (SQLException ex) {
            	System.out.println("Error connecting to SQL or executing query");
            }
        }
		
    	return result;
    }
    
    
}