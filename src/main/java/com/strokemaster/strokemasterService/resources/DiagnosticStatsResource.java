package com.strokemaster.strokemasterService.resources;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.json.JSONException;
import org.json.JSONObject;

import com.codahale.metrics.annotation.Timed;

/**
 * Login resource 
 * @author Sean
 *
 */
@Path("/diagnosticStats")
@Produces(MediaType.APPLICATION_JSON)
public class DiagnosticStatsResource {
    @SuppressWarnings("unused")
	private final String template; 
    
    public DiagnosticStatsResource(String template) {
        this.template = template;
    }

    /**
     * Takes userID and returns diagnostic stats data for that user
     * @param userID
     * @return
     */
    @GET
    @Timed
    public String getDiagnosticStats(@QueryParam("userID") String userID) {
         			
    	JSONObject diagnosticStatsObject = new JSONObject();
    	/*String [] shotType = {"Overall", "Driving", "Approach", "Short Game", "Putter"};
    	int [] index = {72, 72, 80, 82, 85}, values = {80, 75, 85, 84, 95}, excess = {8, 3, 5, 2, 10};
    	
    	for (int i = 0; i < shotType.length; i ++)
    	{
    		JSONObject statObject = new JSONObject();
    		try {
	    		statObject.put("index", index[i]);
	    		statObject.put("value", values[i]);
	    		statObject.put("excess", excess[i]);
	    		diagnosticStatsObject.put(shotType[i], statObject);
    		} catch (JSONException e) {
    			e.printStackTrace();
    		}   	    		
    	}*/
    	
    	try {
			diagnosticStatsObject.put("Round Values", getStats(userID));
		} catch (JSONException e) {
			e.printStackTrace();
		}
    	
    	return getStats(userID);
    	//return diagnosticStatsObject.toString();
    }
    
    /**
     * Calculates all diangostic stats based on the values for the user in the DB
     * @param userID
     * @return
     */
    public String getStats(String userID){
    	
    	// The connection details for the mysql database
		String url = "jdbc:mysql://strokemasterdb-dev.czhvjbu7ieji.us-west-2.rds.amazonaws.com:3306/";
		String userName = "struckmaster";
		String password = "MasterBlaster";
		String dbName = "StrokemasterDBDev";
		String driver = "com.mysql.jdbc.Driver";
		JSONObject diagnoStats = new JSONObject(), shotTypeObject = new JSONObject();
				
		// SQL Objects
		Connection conn = null;
		ResultSet rsRound = null, rsShot = null;
		PreparedStatement ps = null;
		
		
		// Counters
		int rounds = 0;
		
		// Overall, Drive, Approach, Short Game, Putting
		int [] indexValues = {0,0,0,0,0}, userValues = {0,0,0,0,0}, excessValues = {0,0,0,0,0};
		
		
		try {
			//get the driver and create a connection
			Class.forName(driver);
            conn = DriverManager.getConnection(url + dbName, userName, password);
            //create query and substitute in parameters
            ps = conn.prepareStatement("SELECT ID, startHole, Par, Score, TotalScore, TotalPar FROM Round WHERE UserID = ?");
            ps.setString(1, userID);
      
            rsRound = ps.executeQuery();
            
            // While there are rounds for the user remaining
            while(rsRound.next()){
            	// Increment the number of rounds (for taking the average values) and add TotalPar to the first index (overall)
            	rounds ++;
            	indexValues[0] = indexValues[0] + Integer.parseInt(rsRound.getString(6));
            	// Calculate the index values for shot components in the round based on the Par values in the course
            	indexValues = calculateIndexes(indexValues, rsRound.getString(3));
            	
            	// Retrieve list of shots from database based on the current RoundID from the round result set
                ps = conn.prepareStatement("SELECT Stroke, Club, startDistance FROM Shot WHERE RoundID = ?");
                ps.setString(1, rsRound.getString(1));
                rsShot = ps.executeQuery();

            	// For every shot in the round, add it to the appropriate ShotType counter in userValues 
                while(rsShot.next()){
                	// Use the shot information to classify it to a specific shot type
                	userValues = calculateUserValues(userValues, rsShot.getInt(1), rsShot.getString(2), rsShot.getInt(3));
                	// Increment the number of total shots
                    userValues[0] = userValues[0] += 1;
                	
                }
            	
                
            	//roundIDs += rsRound.getString(1) + ": " + rsRound.getString(5) + " ";
            	
            }

        } catch (SQLException | ClassNotFoundException ex) {
            //Logger lgr = Logger.getLogger(Version.class.getName());
            //lgr.log(Level.SEVERE, ex.getMessage(), ex);
        	System.out.println("Error connecting to SQL or executing query");

        } finally {
        	//close any open connections
            try {
                if (rsRound != null) {
                    rsRound.close();
                }
                if (rsShot != null) {
                    rsShot.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }

            } catch (SQLException ex) {
            	System.out.println("Error connecting to SQL or executing query");
            }
        }
		
		// Prepare JSON Object
		String [] shotTypes = {"Overall", "Driving", "Approach", "Short Game", "Putting"}; 
		String debug = "Rounds: " + rounds + " ||| Total Par: " + indexValues[0] + " ||| Total Shots: " + userValues[0] + " ||| Shot Type Total: " 
				+ (userValues[1] + userValues[2] + userValues[3] + userValues[4]) + " ||| Score Against Par: " + (userValues[0] - indexValues[0] );
				
		try {
			/*diagnoStats.put("Rounds", rounds);
			diagnoStats.put("Total Par", indexValues[0]);
			diagnoStats.put("Total Shots", userValues[0]);
			diagnoStats.put("Shot Type Total", (userValues[1] + userValues[2] + userValues[3] + userValues[4]));*/
			diagnoStats.put("Debug", debug);
			
			// Go through every shot type, calculate the excess values and put everything into a nested JSON Object in the main object
			for (int zzz = 0; zzz < shotTypes.length; zzz ++)
			{
				shotTypeObject = new JSONObject();
				shotTypeObject.put("Index", indexValues[zzz]);
				shotTypeObject.put("Value", userValues[zzz]);
				excessValues[zzz] = userValues[zzz] - indexValues[zzz];
				shotTypeObject.put("Excess", excessValues[zzz]);
				diagnoStats.put(shotTypes[zzz], shotTypeObject);
			}
			
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
    	return diagnoStats.toString();
    }
   
	private int [] calculateIndexes(int [] indexes, String par)
    {
    	// Split the par string into individual numbers and go through each one to generate index values
    	String [] split = par.split(",");
    	int parValue = 0;
    	for (int ci = 0; ci < split.length; ci ++)
    	{
    		parValue = Integer.parseInt(split[ci]);
    		
    		// Increment the appropriate indexes based on the par value
    		switch(parValue)
    		{
    			// 1 Approach, 1 Short, 1 Putt
    			case 3: indexes[2] = indexes[2] += 1; indexes[3] = indexes[3] += 1; indexes[4] = indexes[4] += 1; break;
    			// 1 Drive, 1 Approach, 1 Short, 1 Putt
    			case 4: indexes[1] = indexes[1] += 1; indexes[2] = indexes[2] += 1; indexes[3] = indexes[3] += 1; indexes[4] = indexes[4] += 1; break;
    			// 1 Drive, 2 Approach, 1 Short, 1 Putt
    			case 5: indexes[1] = indexes[1] += 1; indexes[2] = indexes[2] += 2; indexes[3] = indexes[3] += 1; indexes[4] = indexes[4] += 1; break;
    			default:
    		}
    		
    		
    	}
    	return indexes;
    }
	
	// Determine the shotType based on the given shot Data
	private int[] calculateUserValues(int[] userValues, int stroke, String club, int startDistance)
	{
		// If they use a driver and its the first shot, it's a drive
		if ( club.equals("Driver") && stroke == 1 )
			userValues[1] = userValues[1] += 1;
		// If they use a putter, it' a putt
		else if ( club.equals("Putter") )
			userValues[4] = userValues[4] += 1;
		// If the ball was not driven and was shot from more than 100 yards away, its  an approach shot
		else if ( startDistance >= 100 )
			userValues[2] = userValues[2] += 1;
		// IF the ball is closer than 100 yards and was not a putt, it's a short shot
		else if ( startDistance < 100 )
			userValues[3] = userValues[3] += 1;
		
		return userValues;

	}
    
}
