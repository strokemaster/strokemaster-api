package com.strokemaster.strokemasterService.resources;

import com.google.common.base.Optional;
import com.strokemaster.strokemasterService.core.CourseData;
import com.strokemaster.strokemasterService.core.CourseDataDetailed;
import com.strokemaster.strokemasterService.core.IGolf;
import com.codahale.metrics.annotation.Timed;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

/**
 * Course Data Resource for getting detailed data
 * 
 * @author Greg Morano
 *
 */
@Path("/course-data")
@Produces(MediaType.APPLICATION_JSON)
public class CourseDataResource {
    @SuppressWarnings("unused")
	private final String template; 
    
    public CourseDataResource(String template) {
        this.template = template;
    }

    /**
     * get the course data for a specified course
     * @param json
     * @return
     */
    @GET
    @Timed
    public CourseData getCourseData(@QueryParam("course") Optional<JSONObject> json) {
         	
        String course = sendRequest(IGolf.getIGolfUrl("CourseDetails"));
        JSONObject courseJson = null;;
		try {
			courseJson = new JSONObject(course);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		CourseDataDetailed cs = new CourseDataDetailed(courseJson);
		
		return cs;
        //return new CourseData(courseJson);
    }
    
    /**
     * send a request to iGolf
     * @param url
     * @return iGolf response string
     */
    public String sendRequest(String url){
    	    HttpClient httpClient = new DefaultHttpClient();
    	    JSONObject json = new JSONObject();
    	    String xmldata = null;

    	    //test object
    	    try{
	    	    json.put("id_course", "Thr5Yyjh1Co0");
	    	    json.put("detailLevel", "2");
	    	    json.put("countryFormat", "3");
	    	    json.put("stateFormat", "2");
    	    	
    	    	
    	    }catch(Exception e){
    	    	e.printStackTrace();
    	    }
    	    
    	    try {
    	    	//test output
    	    	System.out.println("IGolf Request:");
    	    	System.out.println(url);
    	    	System.out.println(json);
    	    	
    	    	/*
    	    	 * setup http request to iGolf
    	    	 * entity is the json we send over
    	    	 */
    	        HttpPost request = new HttpPost(url);
    	        StringEntity params =new StringEntity(json.toString(),ContentType.create("application/json"));
    	        request.addHeader("content-type", "application/json");
    	        request.setEntity(params);
    	        HttpResponse response = httpClient.execute(request);
    	        
    	        //test output
    	        System.out.println("IGolf Response:");
    	        
    	        /*
    	         * parse response object into string
    	         */
    	        InputStream is = response.getEntity().getContent();
    	        ByteArrayOutputStream os = new ByteArrayOutputStream();
    	        byte[] buf;
    	        int ByteRead;
    	        buf = new byte[1024];

    	        
    	        while ((ByteRead = is.read(buf, 0, buf.length)) != -1) {
    	            os.write(buf, 0, ByteRead);                     
    	        }

    	        xmldata =  os.toString();
    	        os.close();
    	        is.close();
    	        //test output response
    	        System.out.println(xmldata);
    	        // handle response here...
    	    }catch (Exception ex) {
    	        // handle exception here
    	    } finally {
    	        httpClient.getConnectionManager().shutdown();
    	    }
    	 
    	    return xmldata;
    }
}