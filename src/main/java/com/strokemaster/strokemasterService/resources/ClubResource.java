package com.strokemaster.strokemasterService.resources;


import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.strokemaster.strokemasterService.core.GolfBag;
import com.strokemaster.strokemasterService.core.Result;

/**
 * Resource for creating new users
 * @author Greg Morano
 *
 */
@Path("/clubs")
@Produces(MediaType.APPLICATION_JSON)
public class ClubResource {
	

	@SuppressWarnings("unused")
	private final String template; 
    
    public ClubResource(String template) {
        this.template = template;
    }

    /**
     * Creates a user and returns result
     * @param email
     * @param password
     * @return
     */
    @GET
    @Timed
    public String getClubs(@QueryParam("list") String type) {
    	
    	
    	if(type.equals("all")){
    		return getAllClubs();
    	}
    	else{
    		return getUserClubs(type);
    	}
         	        		
    }
    
    
    @POST
    @Timed
    @JsonIgnoreProperties(ignoreUnknown = true)
    @Consumes(MediaType.APPLICATION_JSON)
    public Result getUser(GolfBag clubs) throws JsonParseException, JsonMappingException, IOException {
        
    	String clubsString = "";
    	for(String s: clubs.getClubs()){
    		clubsString += s + ", ";
    	}
    	clubsString = clubsString.substring(0, clubsString.length()-2);
		return new Result(updateClubs(clubs.getEmail(), clubsString));
		
    }
   
    /**
     * updates User based on clubs selected
     * @param email
     * @param pass
     * @return
     */
    public int updateClubs(String email, String clubs){
    	
    	// The connection details for the mysql database
		String url = "jdbc:mysql://strokemasterdb-dev.czhvjbu7ieji.us-west-2.rds.amazonaws.com:3306/";
		String userName = "struckmaster";
		String password = "MasterBlaster";
		String dbName = "StrokemasterDBDev";
		String driver = "com.mysql.jdbc.Driver";
		
		int result = -1;
		// SQL Objects
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		
		try {
			//connect driver and open connection
			Class.forName(driver);
            conn = DriverManager.getConnection(url + dbName, userName, password);
            //create query and substitute values
            ps = conn.prepareStatement("UPDATE User SET Clubs = ? WHERE Email = ?");
            ps.setString(1, clubs);
            ps.setString(2, email);
            result = ps.executeUpdate();
            

        } catch (SQLException | ClassNotFoundException ex) {
            //Logger lgr = Logger.getLogger(Version.class.getName());
            //lgr.log(Level.SEVERE, ex.getMessage(), ex);
        	System.out.println("Error connecting to SQL or executing query");

        } finally {
        	//clean up any open connections
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }

            } catch (SQLException ex) {
            	System.out.println("Error connecting to SQL or executing query");
            }
        }
		
    	return result;
    }
    
    /**
     * Returns club list for user
     * @param email
     * @return
     */
    public String getUserClubs(String email){
    	
    	// The connection details for the mysql database
		String url = "jdbc:mysql://strokemasterdb-dev.czhvjbu7ieji.us-west-2.rds.amazonaws.com:3306/";
		String userName = "struckmaster";
		String password = "MasterBlaster";
		String dbName = "StrokemasterDBDev";
		String driver = "com.mysql.jdbc.Driver";
		
		String result = "";
		// SQL Objects
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		
		try {
			//connect driver and open connection
			Class.forName(driver);
            conn = DriverManager.getConnection(url + dbName, userName, password);
            //create query and substitute values
            ps = conn.prepareStatement("SELECT Clubs FROM User WHERE Email = ?");
            ps.setString(1, email);

            rs = ps.executeQuery();
            
            while(rs.next() ){
            	result = rs.getString("Clubs");
            }


        } catch (SQLException | ClassNotFoundException ex) {
            //Logger lgr = Logger.getLogger(Version.class.getName());
            //lgr.log(Level.SEVERE, ex.getMessage(), ex);
        	System.out.println("Error connecting to SQL or executing query");

        } finally {
        	//clean up any open connections
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }

            } catch (SQLException ex) {
            	System.out.println("Error connecting to SQL or executing query");
            }
        }
		
    	return result;
    }
    
    /**
     * Returns list of all clubs availble to choose from
     * @return
     */
    public String getAllClubs(){
    	
    	// The connection details for the mysql database
		String url = "jdbc:mysql://strokemasterdb-dev.czhvjbu7ieji.us-west-2.rds.amazonaws.com:3306/";
		String userName = "struckmaster";
		String password = "MasterBlaster";
		String dbName = "StrokemasterDBDev";
		String driver = "com.mysql.jdbc.Driver";
		
		String result = "";
		// SQL Objects
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		
		try {
			//connect driver and open connection
			Class.forName(driver);
            conn = DriverManager.getConnection(url + dbName, userName, password);
            //create query and substitute values
            ps = conn.prepareStatement("SELECT * FROM Club ORDER BY ID");

            rs = ps.executeQuery();
            
            while(rs.next() ){
            	result += rs.getString("clubName") + ", ";
            }
            
            result = result.substring(0, result.length()-2);

        } catch (SQLException | ClassNotFoundException ex) {
            //Logger lgr = Logger.getLogger(Version.class.getName());
            //lgr.log(Level.SEVERE, ex.getMessage(), ex);
        	System.out.println("Error connecting to SQL or executing query");

        } finally {
        	//clean up any open connections
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }

            } catch (SQLException ex) {
            	System.out.println("Error connecting to SQL or executing query");
            }
        }
		
    	return result;
    }
    
    
}