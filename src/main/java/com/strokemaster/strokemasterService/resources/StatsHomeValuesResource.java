package com.strokemaster.strokemasterService.resources;


import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.json.JSONException;
import org.json.JSONObject;

import com.codahale.metrics.annotation.Timed;

/**
 * Login resource 
 * @author sean
 *
 */
@Path("/statsHome")
@Produces(MediaType.APPLICATION_JSON)
public class StatsHomeValuesResource {
    @SuppressWarnings("unused")
	private final String template; 
    
    public StatsHomeValuesResource(String template) {
        this.template = template;
    }

    /**
     * Takes userID and returns the Average Score and number of Rounds in JSON
     * @param userID
     * @return
     */
    @GET
    @Timed
    public String getStatsHomeValues(@QueryParam("userID") String userID) {
         			
    	//String statsHomeValues = "average = " + "85" + "\n";
    	//statsHomeValues += "rounds = 12 \n";
		//statsHomeValues += "userID" + userID + "\n";
    	JSONObject statsHomeValues = new JSONObject();
    	try {
			statsHomeValues.put("average", 85);
			statsHomeValues.put("rounds", 12);
	    	statsHomeValues.put("userID", userID);
		} catch (JSONException e) {
			e.printStackTrace();
		}   	
		
    	
    	return statsHomeValues.toString();

    }
    
    /**
     * Checks the credentials against what is in the database
     * @param email
     * @param pass
     * @return
     */
    public String checkLogin(String email, String pass){
    	
    	// The connection details for the mysql database
		String url = "jdbc:mysql://strokemasterdb-dev.czhvjbu7ieji.us-west-2.rds.amazonaws.com:3306/";
		String userName = "struckmaster";
		String password = "MasterBlaster";
		String dbName = "StrokemasterDBDev";
		String driver = "com.mysql.jdbc.Driver";
		
		String result = "Failed";
		// SQL Objects
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		
		try {
			//get the driver and create a connection
			Class.forName(driver);
            conn = DriverManager.getConnection(url + dbName, userName, password);
            //create query and substitute in parameters
            ps = conn.prepareStatement("SELECT * FROM User WHERE EMAIL = ? AND PASSWORD = ?");
            ps.setString(1, email);
            ps.setString(2, pass);
            rs = ps.executeQuery();
            
            if(rs.next()){
            	result = "Success";
            }

        } catch (SQLException | ClassNotFoundException ex) {
            //Logger lgr = Logger.getLogger(Version.class.getName());
            //lgr.log(Level.SEVERE, ex.getMessage(), ex);
        	System.out.println("Error connecting to SQL or executing query");

        } finally {
        	//close any open connections
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }

            } catch (SQLException ex) {
            	System.out.println("Error connecting to SQL or executing query");
            }
        }
		
    	return result;
    }
    
    
}