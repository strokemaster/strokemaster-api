package com.strokemaster.strokemasterService.resources;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.codahale.metrics.annotation.Timed;
import com.strokemaster.strokemasterService.core.ClubStats;

/**
 * Login resource 
 * @author Greg Morano
 *
 */
@Path("/clubStats")
@Produces(MediaType.APPLICATION_JSON)
public class ClubStatsResource {
    @SuppressWarnings("unused")
	private final String template; 
    
    public ClubStatsResource(String template) {
        this.template = template;
    }

    /**
     * Takes email and password and verifies credentials
     * @param email
     * @param password
     * @return
     */
    @GET
    @Timed
    public ArrayList<ClubStats> getLeaderboard(@QueryParam("userID") String user) {
    	
    	return getStats(user);
    }
    
    /**
     * Checks the credentials against what is in the database
     * @param email
     * @param pass
     * @return
     */
    public ArrayList<ClubStats> getStats(String user){
    	
    	// The connection details for the mysql database
		String url = "jdbc:mysql://strokemasterdb-dev.czhvjbu7ieji.us-west-2.rds.amazonaws.com:3306/";
		String userName = "struckmaster";
		String password = "MasterBlaster";
		String dbName = "StrokemasterDBDev";
		String driver = "com.mysql.jdbc.Driver";
		
		ArrayList<ClubStats> results = new ArrayList<ClubStats>();
		// SQL Objects
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		
		try {
			//get the driver and create a connection
			Class.forName(driver);
            conn = DriverManager.getConnection(url + dbName, userName, password);
            //create query and substitute in parameters
            ps = conn.prepareStatement("SELECT s.UserID, s.Club, count(s.UserID) as numShots, "
            		+ "AVG(s.Yardage) as averageDist, MIN(s.Yardage) as minDist, MAX(s.Yardage) "
            		+ "as maxDist ,c.ID FROM Shot s, Club c WHERE s.UserID ="
            		+ "? AND s.Club = c.clubName GROUP BY s.Club ORDER BY c.ID ASC");
            ps.setString(1, user);
            rs = ps.executeQuery();
            
            
            while(rs.next() ){
            	results.add(  new ClubStats(rs.getString("Club"), rs.getDouble("averageDist"), 
            			rs.getInt("minDist"), rs.getInt("maxDist") ));
            }
            

        } catch (SQLException | ClassNotFoundException ex) {
            //Logger lgr = Logger.getLogger(Version.class.getName());
            //lgr.log(Level.SEVERE, ex.getMessage(), ex);
        	System.out.println("Error connecting to SQL or executing query");

        } finally {
        	//close any open connections
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }

            } catch (SQLException ex) {
            	System.out.println("Error connecting to SQL or executing query");
            }
        }
		
    	return results;
    }
    
    
}
