package com.strokemaster.strokemasterService.resources;

import com.google.common.base.Optional;
import com.strokemaster.strokemasterService.core.CourseScorecard;
import com.strokemaster.strokemasterService.core.IGolf;
import com.codahale.metrics.annotation.Timed;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Course Data Resource for getting detailed data
 * 
 * @author Greg Morano
 *
 */
@Path("/course-scorecard-list")
@Produces(MediaType.APPLICATION_JSON)
public class CourseScorecardListResource {
    @SuppressWarnings("unused")
	private final String template; 
    
    public CourseScorecardListResource(String template) {
        this.template = template;
    }

    /**
     * get the course data for a specified course
     * @param json
     * @return
     */
    @GET
    @Timed
    public ArrayList<CourseScorecard> getCourseData(@QueryParam("courses") Optional<String> courseIds) {
         	
    	String courses = "";
    	JSONObject json = new JSONObject();
    	if(courseIds.isPresent()){
    		courses = courseIds.get();
    		String[] arr = courses.split(",");
    		
    		try{
    	    	json.put("id_courseArray", arr);
    	    	json.put("courseName", 1);
    	    }catch(Exception e){
    	    	e.printStackTrace();
    	    }
    	}
        String course = sendRequest(IGolf.getIGolfUrl("CourseScorecardList"), json);
        JSONObject courseJson = null;
        ArrayList<CourseScorecard> results = new ArrayList<CourseScorecard>();
		try {
			courseJson = new JSONObject(course);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		CourseScorecard cs = null;
		int count  = 0;
		try {
			while(!courseJson.getJSONArray("courseScorecardList").isNull(count)){
		
				cs = new CourseScorecard(courseJson.getJSONArray("courseScorecardList").getJSONObject(count));
				results.add(cs);
				count++;
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
				
		return results;
    }
    
    /**
     * send a request to iGolf
     * @param url
     * @return iGolf response string
     */
    public String sendRequest(String url, JSONObject json){
    	    HttpClient httpClient = new DefaultHttpClient();
    	    //JSONObject json = new JSONObject();
    	    String xmldata = null;

    	    //test object
    	    /*try{
	    	    //json.put("id_course", "Thr5Yyjh1Co0");
	    	    String[] arr = {"Thr5Yyjh1Co0", "MhBSMSojhZU3"};
    	    	json.put("id_courseArray", arr);
    	    	json.put("courseName", 1);
    	    }catch(Exception e){
    	    	e.printStackTrace();
    	    }*/
    	    
    	    try {
    	    	//test output
    	    	System.out.println("IGolf Request:");
    	    	System.out.println(url);
    	    	System.out.println(json);
    	    	
    	    	/*
    	    	 * setup http request to iGolf
    	    	 * entity is the json we send over
    	    	 */
    	        HttpPost request = new HttpPost(url);
    	        StringEntity params =new StringEntity(json.toString(),ContentType.create("application/json"));
    	        request.addHeader("content-type", "application/json");
    	        request.setEntity(params);
    	        HttpResponse response = httpClient.execute(request);
    	        
    	        //test output
    	        System.out.println("IGolf Response:");
    	        
    	        /*
    	         * parse response object into string
    	         */
    	        InputStream is = response.getEntity().getContent();
    	        ByteArrayOutputStream os = new ByteArrayOutputStream();
    	        byte[] buf;
    	        int ByteRead;
    	        buf = new byte[1024];

    	        
    	        while ((ByteRead = is.read(buf, 0, buf.length)) != -1) {
    	            os.write(buf, 0, ByteRead);                     
    	        }

    	        xmldata =  os.toString();
    	        os.close();
    	        is.close();
    	        //test output response
    	        System.out.println(xmldata);
    	        // handle response here...
    	    }catch (Exception ex) {
    	        // handle exception here
    	    } finally {
    	        httpClient.getConnectionManager().shutdown();
    	    }
    	 
    	    return xmldata;
    }
}