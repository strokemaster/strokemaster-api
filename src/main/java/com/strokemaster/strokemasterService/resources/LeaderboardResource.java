package com.strokemaster.strokemasterService.resources;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.codahale.metrics.annotation.Timed;
import com.strokemaster.strokemasterService.core.Leaderboard;

/**
 * Login resource 
 * @author Sean
 *
 */
@Path("/leaderboard")
@Produces(MediaType.APPLICATION_JSON)
public class LeaderboardResource {
    @SuppressWarnings("unused")
	private final String template; 
    
    public LeaderboardResource(String template) {
        this.template = template;
    }

    /**
     * Takes email and password and verifies credentials
     * @param email
     * @param password
     * @return
     */
    @GET
    @Timed
    public ArrayList<Leaderboard> getLeaderboard() {
         			
    	/*JSONObject leaderboardObject = new JSONObject();
    	String [] users = {"Sean", "Gerry", "JJ", "Greg"};
    	int [] scores = {72, 80, 82, 85}, rounds = {5, 10, 20, 5};
    	
    	for (int i = 0; i < users.length; i ++)
    	{
    		JSONObject leaderObject = new JSONObject();
    		String leaderID = "user " + (i+1);
    		try {
	    		leaderObject.put("user", users[i]);
	    		leaderObject.put("scores", scores[i]);
	    		leaderObject.put("rounds", rounds[i]);
	    		leaderboardObject.put(leaderID, leaderObject);
    		} catch (JSONException e) {
    			e.printStackTrace();
    		}   	    		
    	}
    	
  
    	return leaderboardObject.toString();*/
    	
    	return getLeaders();
    }
    
    /**
     * Checks the credentials against what is in the database
     * @param email
     * @param pass
     * @return
     */
    public ArrayList<Leaderboard> getLeaders(){
    	
    	// The connection details for the mysql database
		String url = "jdbc:mysql://strokemasterdb-dev.czhvjbu7ieji.us-west-2.rds.amazonaws.com:3306/";
		String userName = "struckmaster";
		String password = "MasterBlaster";
		String dbName = "StrokemasterDBDev";
		String driver = "com.mysql.jdbc.Driver";
		
		ArrayList<Leaderboard> results = new ArrayList<Leaderboard>();
		// SQL Objects
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		
		try {
			//get the driver and create a connection
			Class.forName(driver);
            conn = DriverManager.getConnection(url + dbName, userName, password);
            //create query and substitute in parameters
            ps = conn.prepareStatement("SELECT UserID, count(UserID) as numRounds, "
            		+ "AVG(TotalScore) as averageScore FROM Round GROUP BY UserID ORDER BY "
            		+ "averageScore asc LIMIT 10");
      
            rs = ps.executeQuery();
            
            
            while(rs.next() ){
            	results.add(  new Leaderboard(rs.getDouble("averageScore"),rs.getInt("numRounds"), rs.getString("UserID") ));
            }
            

        } catch (SQLException | ClassNotFoundException ex) {
            //Logger lgr = Logger.getLogger(Version.class.getName());
            //lgr.log(Level.SEVERE, ex.getMessage(), ex);
        	System.out.println("Error connecting to SQL or executing query");

        } finally {
        	//close any open connections
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }

            } catch (SQLException ex) {
            	System.out.println("Error connecting to SQL or executing query");
            }
        }
		
    	return results;
    }
    
    
}
