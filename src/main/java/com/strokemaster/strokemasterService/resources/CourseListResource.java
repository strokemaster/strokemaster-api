package com.strokemaster.strokemasterService.resources;


import com.strokemaster.strokemasterService.core.Course;
import com.strokemaster.strokemasterService.core.CourseList;
import com.strokemaster.strokemasterService.core.IGolf;
import com.codahale.metrics.annotation.Timed;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Resource for getting a list of courses to
 * send to the IOS layer
 * 
 * @author Greg Morano
 *
 */
@Path("/course-list")
@Produces(MediaType.APPLICATION_JSON)
public class CourseListResource {
    @SuppressWarnings("unused")
	private final String template;
    @SuppressWarnings("unused")
	private final AtomicLong counter;
    
   
    /**
     * initialize the resource with any default values
     * @param template
     */
    public CourseListResource(String template) {
        this.template = template;
        //this.defaultName = defaultName;
        this.counter = new AtomicLong();
    }

    /**
     * method that gets called on get request
     * returns a list of courses
     * @param json
     * @return
     */
    @GET
    @Timed
    public CourseList getCourseList(@QueryParam("lon") String lon, @QueryParam("lat") String lat) {
        
    	/*
    	 * sends a request and gets the response
    	 * right now its fixed but will be based off
    	 * of user input that will be parsed
    	 */
        String data = sendRequest(IGolf.getIGolfUrl("CourseList"),lon, lat);
        ArrayList<Course> list = new ArrayList<Course>();
        
        JSONObject jsonList;
        int tot = 0;
        try {
        	/*
        	 * populates the courseList object
        	 */
			jsonList = new JSONObject(data);
			tot = jsonList.getInt("totalCourses");
			JSONArray courses = jsonList.getJSONArray("courseList");
			/*
			 * iterates through each course and creates a course to add to the list
			 */
			for(int i = 0; i < tot; i++){
				list.add(new Course((JSONObject)courses.get(i)));
				
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return new CourseList(list, tot);
    }
    
    /**
     * send a request to iGolf
     * @param url
     * @return iGolf response string
     */
    public String sendRequest(String url, String lon, String lat){
    	    HttpClient httpClient = new DefaultHttpClient();
    	    JSONObject json = new JSONObject();
    	    String xmldata = null;
    	    
    	    //test object
    	    try{
	    	    /*json.put("id_course", "i9-KMmw4Gpla");
	    	    json.put("detailLevel", "2");
	    	    json.put("countryFormat", "3");
	    	    json.put("stateFormat", "2");*/
    	    	json.put("active", "1");
	    	    json.put("page", "1");
	    	    json.put("resultsPerPage", "6");
	    	    json.put("countryFormat", "4");
	    	    json.put("stateFormat", "4");
	    	    json.put("deals" , "1");
	    	    json.put("radius", "100");
	    	    //poughkeepsie lat long
	    	    //json.put("referenceLatitude", "41.7003710");
	    	    //json.put("referenceLongitude", "-73.9209700");
	    	    //league city lat long
	    	    if(lat == null || lon == null){
		    	    json.put("referenceLatitude", "29.5415104");
		    	    json.put("referenceLongitude", "-95.0623143");
		    	    System.err.println("Latitude and Longitude not given.  "
		    	    		+ "Defaulting to preset location for testing.  "
		    	    		+ "This is an error in getting gps points from webservice call");
	    	    }
	    	    else{
	    	    	json.put("referenceLatitude", lat);
		    	    json.put("referenceLongitude", lon);
	    	    }
	    	    //json.put("city", "Poughkeepsie");
    	    }catch(Exception e){
    	    	e.printStackTrace();
    	    }
    	    
    	    try {
    	    	//test output
    	    	System.out.println("IGolf Request:");    	    	
    	    	System.out.println(json);
    	    	
    	    	/*
    	    	 * setup http request to iGolf
    	    	 * entity is the json we send over
    	    	 */
    	        HttpPost request = new HttpPost(url);
    	        StringEntity params =new StringEntity(json.toString(),ContentType.create("application/json"));
    	        request.addHeader("content-type", "application/json");
    	        request.setEntity(params);
    	        HttpResponse response = httpClient.execute(request);
    	        
    	        //test output
    	        System.out.println("IGolf Response:");
    	        
    	        /*
    	         * parse response object into string
    	         */
    	        InputStream is = response.getEntity().getContent();
    	        ByteArrayOutputStream os = new ByteArrayOutputStream();
    	        byte[] buf;
    	        int ByteRead;
    	        //int totalSize = 0;
    	        buf = new byte[1024];

    	        while ((ByteRead = is.read(buf, 0, buf.length)) != -1) {
    	            os.write(buf, 0, ByteRead);
    	            //totalSize += ByteRead;                      
    	        }

    	        xmldata =  os.toString();
    	        os.close();
    	        is.close();
    	        //test output of response
    	        System.out.println(xmldata);
    	        // handle response here...
    	    }catch (Exception ex) {
    	        // handle exception here
    	    } finally {
    	        httpClient.getConnectionManager().shutdown();
    	    }
    	    return xmldata;
    }
}