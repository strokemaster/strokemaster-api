package com.strokemaster.strokemasterService.resources;


import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.strokemaster.strokemasterService.core.CourseRound;
import com.strokemaster.strokemasterService.core.HoleShots;

/**
 * Resource for creating new users
 * @author Greg Morano
 *
 */
@Path("/end-round")
@Produces(MediaType.APPLICATION_JSON)
public class EndRoundResource {
	

	@SuppressWarnings("unused")
	private final String template; 
    
    public EndRoundResource(String template) {
        this.template = template;
    }

    /**
     * Creates a user and returns result
     * @param email
     * @param password
     * @return
     */
    @POST
    @Timed
    @JsonIgnoreProperties(ignoreUnknown = true)
    @Consumes(MediaType.APPLICATION_JSON)
    public int getUser(CourseRound data) {
    
    	System.out.println(data);

		return process(data);
    }
   
    /**
     * parses end round data and sends it to the database
     * @param data
     * @return
     */
    public int process(CourseRound data){
    	
    	// The connection details for the mysql database
		String url = "jdbc:mysql://strokemasterdb-dev.czhvjbu7ieji.us-west-2.rds.amazonaws.com:3306/";
		String userName = "struckmaster";
		String password = "MasterBlaster";
		String dbName = "StrokemasterDBDev";
		String driver = "com.mysql.jdbc.Driver";
		
		int result = -1;
		// SQL Objects
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		System.out.println(data);
		
		
		
		try {
			//connect driver and open connection
			Class.forName(driver);
            conn = DriverManager.getConnection(url + dbName, userName, password);
            //create query and substitute values
            ps = conn.prepareStatement("INSERT INTO Round(ID, UserID, CourseName, Par, Score, TotalPar, TotalScore, StartHole, TotalHoles, Date) VALUES(?,?,?,?,?,?,?,?,?,?)");
            Date now = new Date(System.currentTimeMillis());
            SimpleDateFormat sdf = new SimpleDateFormat("ddMyyyyhhmmss");
            String roundID = data.getCourseId() + sdf.format(now) ;
            
            ps.setString(1, roundID);
            ps.setString(2, data.getUserId());
            ps.setString(3, data.getCourseId());
            ps.setString(4, data.getPar());
            ps.setString(5, data.getScore());
            ps.setInt(6, data.getTotalPar());
            ps.setInt(7,data.getTotalScore());
            ps.setInt(8, data.getStartHole());
            ps.setInt(9, data.getHoles().size());
            ps.setDate(10, now);
            
            result = ps.executeUpdate();
            
            //successful insert
            if(result > 0){
            	ArrayList<HoleShots> shots = data.getHoles();
            	for(int i = 0; i < shots.size(); i++){
            		
            		
            		HoleShots shot = shots.get(i);
            		String [] clubs = shot.getClubs();
            		int [] endDists= shot.getEndDist();
            		String [] endPoints = shot.getEndPoint();
            		int [] startDists = shot.getStartDist();
            		String [] startPoints = shot.getStartPoint();
            		String [] terrains = shot.getTerrain();
            		int [] yardages = shot.getYardages();
            		//int par = shot.getPar();
            		
        			for(int y = 0; y < yardages.length; y++){
        				
	            		ps = conn.prepareStatement("INSERT INTO Shot(RoundID, CourseName, UserID, Hole, Stroke, startPoint, endPoint, startDistance, endDistance, Yardage, Terrain, Club) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)");
	                    ps.setString(1, roundID);
	                    ps.setString(2, data.getCourseId());
	                    ps.setString(3, data.getUserId());
	                    ps.setInt(4, i+1);
	                    ps.setInt(5, y+1);
	                    ps.setString(6, startPoints[y]);
	                    ps.setString(7, endPoints[y]);
	                    ps.setInt(8, startDists[y]);
	                    ps.setInt(9, endDists[y]);
	                    ps.setInt(10, yardages[y]);
	                    ps.setString(11, terrains[y]);
	                    ps.setString(12, clubs[y]);
	                    
	                    result = ps.executeUpdate();
        			}
            	}
            }
            //Round insertion failed something is wrong
            else{
            	System.err.println("Round insertion failed");
            }
            

        } catch (SQLException | ClassNotFoundException ex) {
        	System.out.println("Error connecting to SQL or executing query");

        } finally {
        	//clean up any open connections
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }

            } catch (SQLException ex) {
            	System.out.println("Error connecting to SQL or executing query");
            }
            
        }
		
    	return result;
    }
    
    
}