package com.strokemaster.strokemasterService.resources;

import com.google.common.base.Optional;
import com.strokemaster.strokemasterService.core.HoleVectorData;
import com.strokemaster.strokemasterService.core.IGolf;
import com.codahale.metrics.annotation.Timed;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;


import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Resource to return course vector data
 * 
 * @author Greg Morano
 *
 */
@Path("/vector-data")
@Produces(MediaType.APPLICATION_JSON)
public class VectorDataResource {
    @SuppressWarnings("unused")
	private final String template; 
    
    public VectorDataResource(String template) {
        this.template = template;
    }

    /**
     * get the course data for a specified course
     * @param json
     * @return
     */
    @GET
    @Timed
    public ArrayList<HoleVectorData> getVectorData(@QueryParam("id") Optional<String> courseId) {
         	
        String course = sendRequest(IGolf.getIGolfUrl("CourseGPSVectorDetails"),courseId);
        ArrayList<HoleVectorData> holes = new ArrayList<HoleVectorData>();
        HoleVectorData hole = null;
        JSONObject courseJson = null;
		try {
			courseJson = new JSONObject(course);
			for(int i = 0; i < courseJson.getJSONObject("vectorGPSObject").getInt("HoleCount"); i++){
				hole = new HoleVectorData( courseJson.getJSONObject("vectorGPSObject"), i);
				holes.add(hole);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		//Test for seeing hole data
		/*	FileWriter file = null;
	        try {
	        	file = new FileWriter("/Users/greg/Desktop/hole1.txt");
	            file.write(courseJson.toString());
	            System.out.println("Successfully Copied JSON Object to File...");

	 
	        } catch (IOException e) {
	            e.printStackTrace();
	 
	        } finally {
	            try {
					file.flush();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	            try {
					file.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	        }*/
		
		return holes;
    }
    
    /**
     * send a request to iGolf
     * @param url
     * @return iGolf response string
     */
    public String sendRequest(String url, Optional<String> courseId){
    	    HttpClient httpClient = new DefaultHttpClient();
    	    JSONObject json = new JSONObject();
    	    String xmldata = null;
    	    String myId = null;
    	    
    	    if(courseId.isPresent()){
    	    	myId = courseId.get();
    	    }
    	    else{
    	    	myId = "Thr5Yyjh1Co0";
    	    }
    	    //test object
    	    try{
    	    	//mcCann's
	    	    json.put("id_course", myId);	
	    	    //casperkill
	    	    //json.put("id_course", "MhBSMSojhZU3");
    	    	
    	    }catch(Exception e){
    	    	e.printStackTrace();
    	    }
    	    
    	    try {
    	    	//test output
    	    	System.out.println("IGolf Request:");
    	    	System.out.println(url);
    	    	System.out.println(json);
    	    	
    	    	/*
    	    	 * setup http request to iGolf
    	    	 * entity is the json we send over
    	    	 */
    	        HttpPost request = new HttpPost(url);
    	        StringEntity params =new StringEntity(json.toString(),ContentType.create("application/json"));
    	        request.addHeader("content-type", "application/json");
    	        request.setEntity(params);
    	        HttpResponse response = httpClient.execute(request);
    	        
    	        //test output
    	        System.out.println("IGolf Response:");
    	        
    	        /*
    	         * parse response object into string
    	         */
    	        InputStream is = response.getEntity().getContent();
    	        ByteArrayOutputStream os = new ByteArrayOutputStream();
    	        byte[] buf;
    	        int ByteRead;
    	        buf = new byte[1024];

    	        
    	        while ((ByteRead = is.read(buf, 0, buf.length)) != -1) {
    	            os.write(buf, 0, ByteRead);                     
    	        }

    	        xmldata =  os.toString();
    	        os.close();
    	        is.close();
    	        //test output response
    	        System.out.println(xmldata);
    	        // handle response here...
    	    }catch (Exception ex) {
    	        // handle exception here
    	    } finally {
    	        httpClient.getConnectionManager().shutdown();
    	    }
    	 
    	    return xmldata;
    }
}