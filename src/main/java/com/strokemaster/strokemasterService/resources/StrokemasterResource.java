package com.strokemaster.strokemasterService.resources;

import com.strokemaster.strokemasterService.core.Saying;
import com.google.common.base.Optional;
import com.codahale.metrics.annotation.Timed;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Hello World example resource
 *
 */
@Path("/hello-world")
@Produces(MediaType.APPLICATION_JSON)
public class StrokemasterResource {
    private final String template;
    private final String defaultName;
    private final AtomicLong counter;

    public StrokemasterResource(String template, String defaultName) {
        this.template = template;
        this.defaultName = defaultName;
        this.counter = new AtomicLong();
    }

    /**
     * creates String if given value otherwise uses default 
     * values in the config.yml
     * 
     * @param name
     * @return new saying object to the caller
     */
    @GET
    @Timed
    public Saying sayHello(@QueryParam("name") Optional<String> name) {
        final String value = String.format(template, name.or(defaultName));
        return new Saying(counter.incrementAndGet(), value);
    }
}