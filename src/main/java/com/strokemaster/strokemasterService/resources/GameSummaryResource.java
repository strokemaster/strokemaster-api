package com.strokemaster.strokemasterService.resources;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.json.JSONException;
import org.json.JSONObject;

import com.codahale.metrics.annotation.Timed;

/**
 * Login resource 
 * @author Sean
 *
 */
@Path("/gameSummary")
@Produces(MediaType.APPLICATION_JSON)
public class GameSummaryResource {
    @SuppressWarnings("unused")
	private final String template; 
    
    public GameSummaryResource(String template) {
        this.template = template;
    }

    /**
     * Takes userID and returns diagnostic stats data for that user
     * @param userID
     * @return
     */
    @GET
    @Timed
    public String getGameSummary(@QueryParam("userID") String userID) {
    	return getStats(userID);
    }
    
    /**
     * Get the shotType specific stats for the given userID
     * @param userID
     * @return
     */
    public String getStats(String userID){
    	
    	// The connection details for the mysql database
		String url = "jdbc:mysql://strokemasterdb-dev.czhvjbu7ieji.us-west-2.rds.amazonaws.com:3306/";
		String userName = "struckmaster";
		String password = "MasterBlaster";
		String dbName = "StrokemasterDBDev";
		String driver = "com.mysql.jdbc.Driver";
		
		JSONObject gameSummaryObject = new JSONObject();
		
		// SQL Objects
		Connection conn = null;
		ResultSet rsRound = null, rsShot = null;
		PreparedStatement ps = null;
		
		@SuppressWarnings("unused")
		int rounds = 0, superTotalScore = 0, totalHolesUserValue = 0;
		String [] shotTypes = {"Overall", "Driving", "Approach", "Short Game", "Putting"};
		// Overall, Drive, Approach, Short Game, Putting
		int [] shotCounters = {0,0,0,0,0};
		double [] shotSums = {0,0,0,0,0};
		int shotType = 0;
        @SuppressWarnings("unused")
		String debugString = "";
        String scores = "";

		
		try {
			 
			
			 Class.forName(driver);
  			 conn = DriverManager.getConnection(url + dbName, userName, password);
	         //create query and substitute in parameters
	         ps = conn.prepareStatement("SELECT ID, startHole, Par, Score, TotalScore, TotalPar, TotalHoles FROM Round WHERE UserID = ?");
             ps.setString(1, userID);
      
             rsRound = ps.executeQuery();
             
             
             // While there are rounds for the user remaining
             while(rsRound.next()){
            	// Increment the number of rounds (for taking the average values) and the number of holes
            	rounds ++;
            	totalHolesUserValue += rsRound.getInt(7);
            	if(scores.equals("")){
            		scores += rsRound.getString(5);
            	}
            	else{
            		scores += "," +rsRound.getString(5);
            	}
            	
            	// Retrieve list of shots from database based on the current RoundID from the round result set
                ps = conn.prepareStatement("SELECT Stroke, Club, startDistance, endDistance FROM Shot WHERE RoundID = ?");
                ps.setString(1, rsRound.getString(1));
                rsShot = ps.executeQuery();

            	// For every shot in the round, add it to the appropriate ShotType counter in userValues 
                while(rsShot.next()){
                	// Increment the overall counter for every shot
                	shotCounters[0] = shotCounters[0] += 1;
                	
                	// Use Stroke, Club and startDistance to classify it to a specific shot type and increment the appropriate spot in the array
                	shotType = calculateShotType(rsShot.getInt(1), rsShot.getString(2), rsShot.getInt(3));
                	shotCounters[shotType] = shotCounters[shotType] += 1;
                	
                	// Calculate the shotValue based on shotType, startDistance and endDistance
                	shotSums = calculateShotValue(shotSums, shotType, rsShot.getDouble(3), rsShot.getDouble(4)); 

                }
            	//roundIDs += rsRound.getString(1) + ": " + rsRound.getString(5) + " ";
            	
	         }
             // After all shots have been added up, calculate the average for every type and add it to the JSONObject
             for (int i = 0; i < shotTypes.length; i ++)
             {
	             debugString += shotTypes[i] + " Counter: " + shotCounters[i] + " Sum: " + shotSums[i] + " ";

            	 // Avoid Divide by zero errors =) 
            	 if (shotCounters[i] != 0)
            	 {
	            	 try {
						gameSummaryObject.put(shotTypes[i], Math.round(shotSums[i] / shotCounters[i]));
					} catch (JSONException e) {
					}
             	 }
             }

             try {
					gameSummaryObject.put("Scores", scores);
				} catch (JSONException e) {
				}
             

        } catch (SQLException | ClassNotFoundException ex) {
            //Logger lgr = Logger.getLogger(Version.class.getName());
            //lgr.log(Level.SEVERE, ex.getMessage(), ex);
        	System.out.println("Error connecting to SQL or executing query");

        } finally {
        	//close any open connections
            try {
                if (rsRound != null) {
                    rsRound.close();
                }
                if (rsShot != null) {
                    rsShot.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }

            } catch (SQLException ex) {
            	System.out.println("Error connecting to SQL or executing query");
            }
        }
		// Add a debugging string onto the return object when this isn't working
		/*try {
			gameSummaryObject.put("Debug", debugString);
		} catch (JSONException e) {

		}*/
		//return debugString;
		
    	return gameSummaryObject.toString();
    }

// Determine the shotType based on the given shot Data and return the position in the counter array (1-4) that needs to be incremented
 	private int calculateShotType(int stroke, String club, int startDistance)
 	{
 		// If they use a driver and its the first shot, it's a drive
 		if ( club.equals("Driver") && stroke == 1 )
 			return 1;
 		// If they use a putter, it' a putt
 		else if ( club.equals("Putter") )
 			return 4;
 		// If the ball was not driven and was shot from more than 100 yards away, its  an approach shot
 		else if ( startDistance >= 100 )
 			return 3;
 		// IF the ball is closer than 100 yards and was not a putt, it's a short shot
 		else if ( startDistance < 100 )
 			return 2;
 		
 		return 0;

 	}
 	
 	// Calcualte the value for each shot and add it to the appropriate sum and the overall sum so that it can be averaged later
 	
 	private double[] calculateShotValue(double[] shotSums, int shotType, double startDistance, double endDistance) 
 	{
 		// Stupid putting logic
 		if ( shotType == 4 )
 		{
 			// If the putt went in, it gets 100%, otherwise the value array is returned without a change
 			if ( endDistance == 0)
 			{
 				shotSums[4] = shotSums[4] += 100;
 				//Add it to the overall value too
 				shotSums[0] = shotSums[0] += 100;

 			}
 			return shotSums;
 		}
 		double shotValue = 0;
 		
 		// Even better driving logic - a drive of 250 yards gets a score of 100.  Big bonus potential on the 300 + yard drives
 		if (shotType == 1)
 			shotValue = ((startDistance - endDistance) / 300.0) * 100;
 		else
 			shotValue = ((startDistance - endDistance) / startDistance) * 100;
 		shotSums[shotType] = shotSums[shotType] += shotValue;
 		shotSums[0] = shotSums[0] += shotValue;
 		
 		return shotSums;
	}
    
    
}
